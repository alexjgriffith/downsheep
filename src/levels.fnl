(local levels {})

(local quips (require :quips))

(tset levels :list
      {"data-tutorial-1" {:name "data-tutorial-1"
                          :loading-screen "Press 'n' if you need me."
                          :hint "They say the grass is always greener on the other side"
                          :display "Tutorial 1"
                          :description "Tutorial level."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-tutorial-2"
                          }
       "data-tutorial-2" {:name "data-tutorial-2"
                          :display "Tutorial 2"
                          :loading-screen "The yellow pad will set me free."
                          :hint "They say standing on like opens like."
                          :description "Tutorial level."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-tutorial-3"}
       "data-tutorial-3" {:name "data-tutorial-3"
                          :display "Tutorial 3"
                          :loading-screen "Dumbbells are the weight for me!"
                          :hint "Is that a dumbel on the ground?"
                          :description "Tutorial level."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-tutorial-4"}
       "data-tutorial-4" {:name "data-tutorial-4"
                          :display "Tutorial 4"
                          :loading-screen "Jumping through 'SPACE' and time."
                          :hint "With the feather we can both jump."
                          :description "Tutorial level."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-tutorial-5"}
       "data-tutorial-5" {:name "data-tutorial-5"
                          :display "Tutorial 5"
                          :loading-screen "There are no trees in 'SPACE'"
                          :hint "This axe is 'SPACE' age."
                          :description "Tutorial level."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-tutorial-6"}
       "data-tutorial-6" {:name "data-tutorial-6"
                          :display "Tutorial 6"
                          :hint "I can pick em all!"
                          :loading-screen "What you have I also have."
                          :description "Collect the Flowers."
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 6
                          :next-level "data-level-1"}
       "data-level-1" {:name "data-level-1"
                       :display "Level 1"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-2"}
       "data-level-2" {:name "data-level-2"
                       :display "Level 2"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-3"}
       "data-level-3" {:name "data-level-3"
                       :display "Level 3"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-4"}
       "data-level-4" {:name "data-level-4"
                       :display "Level 4"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-5"}
       "data-level-5" {:name "data-level-5"
                       :display "Level 5"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-6"}
       "data-level-6" {:name "data-level-6"
                       :display "Level 6"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-7"}
       "data-level-7" {:name "data-level-7"
                       :display "Level 7"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-8"}
       "data-level-8" {:name "data-level-8"
                       :display "Level 8"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-9"}
       "data-level-9" {:name "data-level-9"
                       :display "Level 9"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-10"}
       "data-level-10" {:name "data-level-10"
                       :display "Level 10"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level "data-level-11"}
       "data-level-11" {:name "data-level-11"
                       :display "Level 11"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 6
                       :next-level nil}
       "data-level-12" {:name "data-level-12"
                       :display "Level 12"
                       :hint ""
                       :loading-screen "Now it begins"
                       :description ""
                       :current-time 0
                       :best-time 1000
                       :flowers-collected 0
                       :max-flowers 0
                       :next-level "data-level-1"}
       "data-user-level" {:name "data-user-level"
                          :display "User Level"
                          :hint ""
                          :loading-screen "Have Fun!"
                          :description ""
                          :current-time 0
                          :best-time 1000
                          :flowers-collected 0
                          :max-flowers 0
                          :next-level "data-user-level"}
        })

(fn levels.display [level]
  (. levels :list level :display))

(fn levels.hint [level]
  (. levels :list level :hint))

(fn levels.loading-screen [level]
  (if level
      (do (local next-level (. levels :list level :next-level))
          (. levels :list next-level :loading-screen))
      (. levels :list :data-tutorial-1 :loading-screen))) ;; update on rename

(fn levels.current-time [level]
  (. levels :list level :current-time))

(fn levels.current-time-inc [level dt]
  (tset (. levels :list level) :current-time (+ (. levels :list level :current-time) dt)))

(fn levels.flower-inc [level]
  (tset (. levels :list level) :flowers-collected (+ (. levels :list level :flowers-collected) 1)))

(fn levels.all-flowers []
  (var ret true)
  (each [_key {: flowers-collected : max-flowers} (pairs levels.list)]
    (when (< flowers-collected max-flowers)
      (set ret false)))
  ret
  )

(fn levels.no-flowers []
  (var ret true)
  (each [_key {: flowers-collected : max-flowers} (pairs levels.list)]
    (when (~= flowers-collected 0)
      (set ret false)))
  ret
  )

(fn levels.count-flowers []
  (var ret 0)
  (each [_key {: flowers-collected} (pairs levels.list)]
      (set ret (+ ret flowers-collected)))
  ret)

(fn _G.newmap [name]
  (local map (require :lib.map))
  (local level ((require :level) level _G.assets.sprites.sprites))
  (level:save (map.new) (.. name ".fnl")))

(fn levels.load [state level-name from-clipboard revert]
  (quips.load-level (. levels.list level-name :display))
  (local objects (require :objects))
  (local player (require :prefab-player))
  (local col (require :prefab-col))
  (local camera-update (require :camera))
  (tset state :pointer {:i 0 :j 0})
  (tset state :camera {:x 0 :y 0 :scale (if _G.web 4 4) ;; need to make a bunch of fixes for it to work with 3 :(
                       :top {:w 1280 :h 20 :type :camera}
                       :bottom {:w 1280 :h 20 :type :camera}
                       :left {:w 20 :h 720 :type :camera}
                       :right {:w 20 :h 720 :type :camera}
                       :speed {:x 0 :y 0 :rate 10 :floor 0.1 :max 6 :decay 10 :max-s2 (/ 6 (math.sqrt 2))}
                       })
  (tset state :canvas (love.graphics.newCanvas 1280 720))
  (state.canvas:setFilter :nearest :nearest)
  (tset state :quad (love.graphics.newQuad 0 0 640 360 1280 720))
  (tset state :level ((require :level) (. levels.list level-name :name)
                      _G.assets.sprites.sprites from-clipboard revert))
  (local sheep-1 (. state.level.map.data.players "sheep one"))
  (local sheep-2 (. state.level.map.data.players "sheep two"))
  (tset state :sheep-1 (player.new "sheep one" :white sheep-1.x sheep-1.y))
  (tset state :sheep-2 (player.new "sheep two" :black sheep-2.x sheep-2.y))
  (tset state :sheep-1 :active true)
  (tset state :sheep-2 :active false)
  (tset state :objects (objects.gen-objects state.level.map.data.objects))
  (tset state :icons (objects.gen-icons state.objects))
  (camera-update.game state.camera state.sheep-1 state.sheep-2)
  (tset state :world (col.new state.level.map [state.sheep-1 state.sheep-2] state.objects state.camera))
  (tset state :keys {:left ["left" "a"]
                     :right ["right" "d"]
                     :up ["up" "w"]
                     :down ["down" "s"]
                     :dashcut ["space"]}))

(fn levels.revert [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level nil :revert)
  (tset state :current-level next-level))

(fn levels.load-from-clipboard [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level :load-from-clipboard)
  (tset state :current-level next-level))

(fn levels.reload [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level)
  (tset state :current-level next-level))

(fn levels.next [state]
  (local next-level (or (. levels :list state.current-level :next-level)
                        :data-tutorial-1))
  (levels.unload state)
  (levels.load state next-level)
  (tset state :current-level next-level))

(fn levels.previous [state]
  (var next-level :data-turorial-1)
  (each [key value (pairs levels.list)]
    (when (= value.next-level state.current-level)
      (set next-level key)))
  (levels.unload state)
  (levels.load state next-level)
  (tset state :current-level next-level))

(fn levels.unload [state]
  (local ret {:sheep-1 (lume.clone state.sheep-1)
              :sheep-2 (lume.clone state.sheep-2)
              :objects (lume.clone state.objects)
              :icons (lume.clone state.icons)
              :level (lume.clone state.level)})
  (tset state :sheep-1 nil)
  (tset state :sheep-2 nil)
  (tset state :objects nil)
  (tset state :icons nil)
  (tset state :level nil)
  (tset state :canvas nil)
  (tset state :pointer nil)
  (tset state :quad nil)
  (tset state :world nil)
  (tset state :keys nil)
  ret)

;; (fn levels.reload [state level-state]
;;   (local col (require :prefab-col))
;;   (local camera-update (require :camera))
;;   (tset state :pointer {:i 0 :j 0})
;;   (tset state :camera {:x 0 :y 0 :scale 4
;;                        :top {:w 1280 :h 20 :type :camera}
;;                        :bottom {:w 1280 :h 20 :type :camera}
;;                        :left {:w 20 :h 720 :type :camera}
;;                        :right {:w 20 :h 720 :type :camera}
;;                        })
;;   (tset state :canvas (love.graphics.newCanvas 1280 720))
;;   (state.canvas:setFilter :nearest :nearest)
;;   (tset state :quad (love.graphics.newQuad 0 0 640 360 1280 720))
;;   (tset state :level level-state.level)
;;   (tset state :sheep-1 level-state.sheep-1)
;;   (tset state :sheep-2 level-state.sheep-2)
;;   (tset state :objects level-state.objects)
;;   (tset state :icons level-state.icons)
;;   (camera-update.game state.camera state.sheep-1 state.sheep-2)
;;   (tset state :world (col.new state.level.map [state.sheep-1 state.sheep-2] state.objects state.camera))
;;   (tset state :keys {:left ["left" "a"]
;;                      :right ["right" "d"]
;;                      :up ["up" "w"]
;;                      :down ["down" "s"]
;;                      :dashcut ["space"]})
;;   )

levels
