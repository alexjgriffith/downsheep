(local editor {})

(local buttons (require :lib.buttons))
(local gamestate (require :lib.gamestate))
(local params (require :params))
(local levels (require :levels))

(local bursh-order [:ground :fence :chasm :markers :coins :pads
                    :heavy-pads :gates :decals :trees])

(local brush-canvas {:ground (love.graphics.newCanvas 8 8)
                     :fence (love.graphics.newCanvas 8 8)
                     :chasm (love.graphics.newCanvas 8 8)
                     :markers (love.graphics.newCanvas 8 8)
                     :coins (love.graphics.newCanvas 8 8)
                     :pads (love.graphics.newCanvas 8 8)
                     :heavy-pads (love.graphics.newCanvas 24 24)
                     :gates (love.graphics.newCanvas 8 8)
                     :decals (love.graphics.newCanvas 8 8)
                     :trees (love.graphics.newCanvas 8 8)})

(local brushes {:ground [:ground]
                :fence [:fence]
                :chasm [:chasm]
                :markers [:start :finish]
                :coins [{:type :coin :colour :yellow :unique true}
                        {:type :coin :colour :pink :unique true}
                        {:type :coin :colour :orange :unique true}
                        {:type :coin :colour :blue :unique true}
                        {:type :coin :colour :red :unique true}
                        {:type :coin :colour :white :unique true}]
                :pads [{:type  :pad :colour :yellow :unique true}
                       {:type :pad :colour :pink :unique true}
                       {:type :pad :colour :orange :unique true}
                       {:type :pad :colour :blue :unique true}
                       {:type :pad :colour :red :unique true}
                       {:type :pad :colour :white :unique true}]
                :heavy-pads [{:type  :pad :colour :yellow :heavy true :unique true}
                             {:type :pad :colour :pink    :heavy true :unique true}
                             {:type :pad :colour :orange  :heavy true :unique true}
                             {:type :pad :colour :blue    :heavy true :unique true}
                             {:type :pad :colour :red     :heavy true :unique true}
                             {:type :pad :colour :white   :heavy true :unique true}]
                :gates [{:type :gate :colour :yellow}
                        {:type :gate :colour :pink}
                        {:type :gate :colour :orange}
                        {:type :gate :colour :blue}
                        {:type :gate :colour :red}
                        {:type :gate :colour :white}]
                :decals [{:type :decal :item :axe :unique true}
                         {:type :decal :item :weight :unique true}
                         {:type :decal :item :feather :unique true}]
                :trees  [{:type :tree}]
})

(local type-to-index {:ground :ground :fence :fence
                      :chasm :chasm
                      :markers :markers
                      :coins :colour
                      :pads :colour
                      :heavy-pads :colour :gates :colour
                      :decals :item :trees :trees})

(local brush-index     {:ground 1 :fence 1 :chasm 1 :markers 2 :colour 1 :item 1 :trees 1})
(local brush-index-max {:ground 1 :fence 1 :chasm 1 :markers 2 :colour 6 :item 3 :trees 1})

(var brush-type :ground)

(local elements
       [{:type :button :y 680 :oy -4 :ox 600 :w 60 :h 30 :text "Back" }
        {:type :button :y 10 :oy -10 :ox 539 :w 50 :h 40 :text "G" :brush :ground}
        {:type :button :y 10 :oy -10 :ox 489 :w 50 :h 40 :text "F" :brush :fence}
        {:type :button :y 10 :oy -10 :ox 439 :w 50 :h 40 :text "C" :brush :chasm}
        {:type :button :y 10 :oy -10 :ox 389 :w 50 :h 40 :text "M" :brush :markers}
        {:type :button :y 10 :oy -10 :ox 339 :w 50 :h 40 :text " " :brush :coins}
        {:type :button :y 10 :oy -10 :ox 289 :w 50 :h 40 :text "P" :brush :pads}
        {:type :button :y 10 :oy -10 :ox 239 :w 50 :h 40 :text "H" :brush :heavy-pads}
        {:type :button :y 10 :oy -10 :ox 189 :w 50 :h 40 :text "X" :brush :gates}
        {:type :button :y 10 :oy -10 :ox 139 :w 50 :h 40 :text "D" :brush :decals}
        {:type :button :y 10 :oy -10 :ox 89 :w 50 :h 40 :text "T"  :brush :trees}
        {:type :button :y 10 :oy -10 :ox 39 :w 50 :h 40 :text "[" }
        {:type :button :y 10 :oy -10 :ox -11 :w 50 :h 40 :text "]" }
        {:type :button :y 10 :oy -10 :ox -66 :w 60 :h 40 :text "Save"}
        {:type :button :y 10 :oy -10 :ox -218 :w 244 :h 40 :text "Load from Clipboard"}
        {:type :button :y 10 :oy -10 :ox -390 :w 100 :h 40 :text "Revert"}
        {:type :button :y 10 :oy -10 :ox -490 :w 100 :h 40 :text "Next"}
        {:type :button :y 10 :oy -10 :ox -590 :w 100 :h 40 :text "Previous"}
        ;;{:type :button :y 10 :oy -10 :ox -126 :w 300 :h 40 :text "Copy To Clipboard"}
        ;; {:type :button :y 10 :oy -10 :ox 600 :w 60 :h 30 :text "Load from Clipboard"}

        ])

(local element-font
       {:title  ((. assets.fonts "avara") 100)
        :subtitle  ((. assets.fonts "avara") 40)
        :button  ((. assets.fonts "avara") 20)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local state (require :state))

(fn decrement-brush [brush-type]
  (pp (.. "decrement " brush-type " " (. type-to-index brush-type)))
  (tset brush-index (. type-to-index brush-type)
        (math.max 1 (- (. brush-index (. type-to-index brush-type)) 1))))

(fn increment-brush [brush-type]
  (pp (.. "increment " brush-type " " (. type-to-index brush-type)))
  (tset brush-index (. type-to-index brush-type)
        (math.min (. brush-index-max (. type-to-index brush-type))
                  (+ (. brush-index (. type-to-index brush-type)) 1))))

(fn save [state copy-to-clipboard?]
  (tset (. state.level.map.data.players "sheep one") :x state.sheep-1.x)
  (tset (. state.level.map.data.players "sheep one") :y state.sheep-1.y)
  (tset (. state.level.map.data.players "sheep two") :x state.sheep-2.x)
  (tset (. state.level.map.data.players "sheep two") :y state.sheep-2.y)
  (_G.assets.sounds.click:play)
  (if (love.filesystem.isFused)
      (state.level:save state.level.map
                        (..
                         (love.filesystem.getSaveDirectory)
                         "/"
                         (or state.current-level "data-new-level") ".fnl")
                        copy-to-clipboard?)
      (state.level:save state.level.map
                        (.. "src/" (or state.current-level "data-new-level") ".fnl")
                        copy-to-clipboard?)))

(local element-click
       {"G" (fn [] (set brush-type :ground))
        "F" (fn [] (set brush-type :fence))
        "C" (fn [] (set brush-type :chasm))
        "M" (fn [] (set brush-type :markers))
        " " (fn [] (set brush-type :coins))
        "P" (fn [] (set brush-type :pads))
        "H" (fn [] (set brush-type :heavy-pads))
        "X" (fn [] (set brush-type :gates))
        "D" (fn [] (set brush-type :decals))
        "T" (fn [] (set brush-type :trees))
        "[" (fn [] (decrement-brush brush-type))
        "]" (fn [] (increment-brush brush-type))
        "Back"
        (fn []
          (assets.sounds.page:play)
          (if state.editor
                (do (tset state :editor false)
                    (love.mouse.setVisible false))
                (do (tset state :editor true)
                    (love.mouse.setVisible true))))
        "Save"
        (fn [] (save state :copy-to-clipboard))
        "Load from Clipboard"
        (fn [] (levels.load-from-clipboard state))
        "Revert"
        (fn [] (levels.revert state))
        "Next"
        (fn [] (levels.next state))
        "Previous"
        (fn [] (levels.previous state))
        })

(local element-hover {:button (fn [element x y] :nothing)})

(local ui (buttons elements params element-click element-hover element-font))

(fn editor.mousereleased [self x y button state]
  (local {: level : canvas : quad : world : camera} state)
  (local [cx cy] [(- (/ (- x offsets.x) camera.scale) camera.x)
                  (- (/ (- y offsets.y) camera.scale) camera.y)])
  (local click-up {:i (math.floor (/ cx (* 8 1)))
                   :j (math.floor (/ cy (* 8 1)))
                   :button button})
  (local ui-hit (ui:mousereleased x y button))
  (when (not ui-hit)
    (if (= click-up.button 1)
        (do
          (tset level :brush (. brushes brush-type (. brush-index (. type-to-index brush-type))))
          (local end-y (math.max click-up.j state.click-down.j))
          (local start-y (math.min click-up.j state.click-down.j))
          (local end-x (math.max click-up.i state.click-down.i))
          (local start-x (math.min click-up.i state.click-down.i))
          (if (= :string (type level.brush) )
              (level.add-tile-region level start-x end-x start-y end-y)
              (level.add-tile state.level click-up.i click-up.j true)))
        (level.remove-object state.level cx cy)))
  (tset state :click-down nil))

(fn editor.mousepressed [self x y button state]
  (local {: camera} state)
  (tset state :click-down
         {:i (math.floor (/ (- (/ (- x offsets.x) camera.scale) camera.x) (* 8 1)))
          :j (math.floor (/ (- (/ (- y offsets.y) camera.scale) camera.y) (* 8 1)))
          :button button}))

(fn editor.keypressed [self key code state]
  (match key
    :1 (set brush-type :ground)
    :2 (set brush-type :fence)
    :3 (set brush-type :chasm)
    :4 (set brush-type :markers)
    :5 (set brush-type :coins)
    :6 (set brush-type :pads)
    :7 (set brush-type :heavy-pads)
    :8 (set brush-type :gates)
    :9 (set brush-type :decals)
    :0 (set brush-type :trees)
    "[" (decrement-brush brush-type)
    "]" (increment-brush brush-type)
    :p (save state)
         ;;(state.level.save state.level state.level.map (.. state.current-level ".fnl"))
    :c (save state :copy-to-clipboard)
    :. (levels.next state)
    :l (do
         (pp "loaded from clipboard")
         (local levels (require :levels))
         (levels.load state state.current-level :from-clipboard))

    ))

(fn editor.update [dt]
  (love.mouse.setVisible true)
  (ui:update dt))

(fn update-brushes [state]
  (local params (require :params))
  (local pointer state.pointer)
  (local objects (require :objects))
  (local main-canvas (love.graphics.getCanvas))
  (each [_ brush-type (pairs bursh-order)]
    (local brush (. brushes brush-type (. brush-index (. type-to-index brush-type))))
    (local canvas (. brush-canvas brush-type))
    (love.graphics.push)
    (love.graphics.setCanvas canvas)
    (love.graphics.clear)
    (love.graphics.setColor 1 1 1 1)
    (if (= :string (type brush))
        (if (= :chasm brush)
            (do
              (love.graphics.setColor params.colours.background)
              (love.graphics.rectangle "fill" 0 0 8 8)
              (love.graphics.setColor 1 1 1 1))
         (do (state.level.hover state.level brush pointer.i pointer.j)
            (love.graphics.draw state.level.hover-batch 0 0 0 1)))
        (let [ret (lume.clone brush)]
          (tset ret :x 0)
          (tset ret :y 0)
          (local obj (objects.gen-object ret))
          (tset obj :index 1)
          (obj:draw {:x 0 :y 0 :scale 1} :editor)))
    (love.graphics.pop))
  (love.graphics.setCanvas main-canvas))


(fn editor.draw [state]
  (local objects (require :objects))
  (local camera state.camera)
  (local pointer state.pointer)
  (local params (require :params))
  (local brush (. brushes brush-type (. brush-index (. type-to-index brush-type))))

  (love.graphics.setColor params.colours.red)
  (love.graphics.rectangle "line"
                           (math.floor (* (+ (* pointer.i 8) camera.x) camera.scale))
                           (math.floor (* (+ (* pointer.j 8) camera.y) camera.scale))
                           (* 8 camera.scale) (* 8 camera.scale)
                           8
                           8
                           20)
  ;; headbar
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle "fill" 0 0 1280 40)
  (love.graphics.rectangle "fill" 0 0 74 74)
  (ui:draw) ;; images are overlayed
  ;; Active brush
  (love.graphics.setColor 1 1 1 1)
  (update-brushes state)
  (love.graphics.draw (. brush-canvas brush-type) 20 20 0 4 4)

  (local (w h _) (love.window.getMode))
  (local px (/ params.screen-width 2))
  ;; (local w2 (math.floor (/ w 2)))
  (each [_ element (ipairs elements)]
    (when element.brush
      (local x (- px (+ element.ox (/ element.w 2))))
      (love.graphics.draw (. brush-canvas element.brush) (+ x 9) 4 0 4)
      ))
  (love.graphics.setColor 1 1 1 1))

editor
