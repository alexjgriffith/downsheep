{
  :data {
    :ground {
      :data {
        79 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]
        80 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1]
        81 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1]
        82 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1]
        83 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1]
        84 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1]
        85 [1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 1 1 0 0 0 1]
        86 [1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1]
        87 [1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1]
        88 [1 1 1 1 1 1 1 1 1 1 0 0 0 0 2 2 2 0 0 0 0 0 0 1]
        89 [1 1 1 1 1 1 1 1 1 1 1 1 1 0 2 2 2 0 1 1 0 0 0 1]
        90 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 1 1 1 1 1 1 1]
        91 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]
        92 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 1 1 1 1 1 1 1]
        93 [1 3 0 0 0 0 1 1 1 1 1 1 1 0 2 2 2 0 1 1 4 4 4 1]
        94 [1 3 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 0 0 0 4 4 4 1]
        95 [1 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 4 4 1]
        96 [1 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 4 4 1]
        97 [1 3 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 1 1 4 4 4 1]
        98 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]
      }
      :map {
        0 "ground"
        1 "chasm"
        2 "start"
        3 "fence"
        4 "finish"
      }
      :off-y 30
      :range {
        :maxx 98
        :maxy 54
        :minx 79
        :miny 31
      }
      :type "dense"
    }
    :objects [{
        :colour "yellow"
        :type "pad"
        :unique true
        :x 696
        :y 336
      } {
        :colour "yellow"
        :type "gate"
        :x 752
        :y 344
      } {
        :colour "yellow"
        :type "gate"
        :x 768
        :y 344
      } {
        :colour "pink"
        :type "gate"
        :x 760
        :y 344
      } {
        :colour "pink"
        :heavy true
        :type "pad"
        :unique true
        :x 760
        :y 320
      } {
        :colour "pink"
        :type "gate"
        :x 752
        :y 296
      } {
        :colour "pink"
        :type "gate"
        :x 760
        :y 296
      } {
        :colour "pink"
        :type "gate"
        :x 768
        :y 296
      } {
        :type "tree"
        :x 752
        :y 392
      } {
        :type "tree"
        :x 768
        :y 392
      } {
        :type "tree"
        :x 760
        :y 392
      } {
        :item "axe"
        :type "decal"
        :unique true
        :x 752
        :y 264
      } {
        :colour "orange"
        :type "pad"
        :unique true
        :x 768
        :y 368
      } {
        :colour "orange"
        :type "gate"
        :x 664
        :y 376
      } {
        :colour "orange"
        :type "gate"
        :x 664
        :y 368
      } {
        :colour "orange"
        :type "gate"
        :x 664
        :y 360
      } {
        :item "feather"
        :type "decal"
        :unique true
        :x 640
        :y 360
      } {
        :colour "yellow"
        :type "gate"
        :x 688
        :y 392
      } {
        :colour "yellow"
        :type "gate"
        :x 704
        :y 392
      } {
        :colour "yellow"
        :type "gate"
        :x 696
        :y 392
      } {
        :item "weight"
        :type "decal"
        :unique true
        :x 688
        :y 408
      } {
        :colour "yellow"
        :type "coin"
        :unique true
        :x 752
        :y 304
      } {
        :colour "pink"
        :type "coin"
        :unique true
        :x 768
        :y 328
      } {
        :colour "orange"
        :type "coin"
        :unique true
        :x 688
        :y 328
      } {
        :colour "blue"
        :type "coin"
        :unique true
        :x 712
        :y 416
      } {
        :colour "red"
        :type "coin"
        :unique true
        :x 752
        :y 400
      } {
        :colour "white"
        :type "coin"
        :unique true
        :x 776
        :y 360
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 704
        :y 350.79000421625
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 736
        :y 360
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}