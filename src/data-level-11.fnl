{
  :data {
    :ground {
      :data {
        74 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        75 [0 0 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        76 [0 0 0 0 0 1 2 2 1 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        77 [0 0 2 2 2 2 2 2 1 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        78 [0 2 2 2 2 2 2 2 1 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        79 [0 2 2 0 0 1 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0]
        80 [0 2 2 2 0 1 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0]
        81 [0 2 2 2 0 1 1 1 1 2 2 2 0 0 0 0 0 0 0 2 2 2 2 2 0 0 0]
        82 [0 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 0 0 0]
        83 [0 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0]
        84 [0 0 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0]
        85 [0 0 2 2 2 2 2 2 2 2 2 2 0 0 0 2 2 2 2 2 2 2 2 2 0 0 0]
        86 [0 0 0 0 0 0 0 2 2 2 2 2 0 1 1 1 1 1 1 2 2 2 2 2 0 0 0]
        87 [0 0 0 0 0 0 0 0 0 2 2 2 0 1 4 4 4 4 1 2 2 3 3 3 0 0 0]
        88 [0 0 0 0 0 0 0 0 0 0 0 0 0 1 4 4 4 4 2 2 2 3 3 3 0 0 0]
        89 [0 0 0 0 0 0 2 2 2 0 2 2 2 2 4 4 4 4 2 2 2 3 3 3 3 3 0]
        90 [0 0 0 0 0 0 2 2 2 2 2 2 2 2 4 4 4 4 1 2 2 3 3 3 3 3 0]
        91 [0 0 0 0 0 0 2 2 2 0 2 2 2 2 4 4 4 4 2 2 2 3 3 3 3 3 0]
        92 [0 0 0 0 0 0 0 0 0 0 0 0 0 1 4 4 4 4 2 2 2 3 3 3 0 0 0]
        93 [0 0 0 0 0 0 0 0 0 2 2 2 0 1 4 4 4 4 1 2 2 3 3 3 0 0 0]
        94 [0 0 0 0 0 0 0 0 0 2 2 2 0 1 1 1 1 1 1 2 2 2 2 2 0 0 0]
        95 [0 0 2 2 2 2 2 2 2 2 2 2 0 0 0 2 2 2 2 2 2 2 2 2 0 0 0]
        96 [0 0 2 2 2 2 2 2 2 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0]
        97 [0 0 2 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0]
        98 [0 0 2 2 0 0 0 0 0 2 2 0 0 0 0 0 0 0 0 2 2 2 2 2 0 0 0]
        99 [0 0 2 2 0 0 0 0 0 2 2 2 2 0 0 0 0 0 0 2 2 2 2 2 0 0 0]
        100 [0 0 2 2 0 0 0 0 0 0 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0]
        101 [0 0 2 2 2 0 0 0 0 0 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0]
        102 [0 0 0 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0]
        103 [0 0 0 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0]
        104 [0 0 0 0 0 0 2 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        105 [0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        106 [0 0 0 0 0 0 0 1 1 2 2 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        107 [0 0 0 0 0 0 0 1 2 2 2 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        108 [0 0 0 0 0 0 0 1 2 2 2 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        109 [0 0 0 0 0 0 0 1 2 2 2 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        110 [0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        111 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "fence"
        2 "ground"
        3 "start"
        4 "finish"
      }
      :off-y 24
      :range {
        :maxx 111
        :maxy 51
        :minx 74
        :miny 25
      }
      :type "dense"
    }
    :objects {
      3 {
        :colour "pink"
        :type "gate"
        :x 728
        :y 344
      }
      4 {
        :item "feather"
        :type "decal"
        :unique true
        :x 712
        :y 248
      }
      5 {
        :colour "orange"
        :type "gate"
        :x 720
        :y 272
      }
      7 {
        :colour "yellow"
        :type "pad"
        :unique true
        :x 720
        :y 360
      }
      8 {
        :colour "blue"
        :type "pad"
        :unique true
        :x 720
        :y 288
      }
      9 {
        :colour "blue"
        :type "gate"
        :x 776
        :y 360
      }
      10 {
        :colour "blue"
        :type "gate"
        :x 776
        :y 368
      }
      11 {
        :colour "orange"
        :type "pad"
        :unique true
        :x 808
        :y 288
      }
      12 {
        :colour "pink"
        :type "gate"
        :x 736
        :y 344
      }
      13 {
        :colour "yellow"
        :type "gate"
        :x 712
        :y 344
      }
      14 {
        :colour "yellow"
        :type "gate"
        :x 704
        :y 344
      }
      15 {
        :colour "orange"
        :type "gate"
        :x 792
        :y 280
      }
      16 {
        :colour "orange"
        :type "gate"
        :x 776
        :y 376
      }
      17 {
        :colour "orange"
        :type "gate"
        :x 776
        :y 384
      }
      21 {
        :type "tree"
        :x 776
        :y 352
      }
      22 {
        :type "tree"
        :x 776
        :y 344
      }
      23 {
        :item "axe"
        :type "decal"
        :unique true
        :x 760
        :y 232
      }
      26 {
        :type "tree"
        :x 672
        :y 248
      }
      27 {
        :type "tree"
        :x 680
        :y 248
      }
      29 {
        :colour "yellow"
        :type "gate"
        :x 680
        :y 264
      }
      30 {
        :colour "yellow"
        :type "gate"
        :x 672
        :y 264
      }
      31 {
        :item "weight"
        :type "decal"
        :unique true
        :x 624
        :y 272
      }
      32 {
        :colour "red"
        :type "pad"
        :unique true
        :x 648
        :y 216
      }
      33 {
        :colour "pink"
        :type "gate"
        :x 624
        :y 216
      }
      34 {
        :colour "yellow"
        :type "coin"
        :unique true
        :x 608
        :y 248
      }
      35 {
        :colour "pink"
        :type "coin"
        :unique true
        :x 616
        :y 256
      }
      38 {
        :colour "red"
        :type "gate"
        :x 640
        :y 320
      }
      39 {
        :colour "blue"
        :type "coin"
        :unique true
        :x 632
        :y 256
      }
      40 {
        :colour "red"
        :type "coin"
        :unique true
        :x 640
        :y 248
      }
      41 {
        :colour "white"
        :type "coin"
        :unique true
        :x 624
        :y 232
      }
      42 {
        :colour "pink"
        :type "gate"
        :x 688
        :y 264
      }
      43 {
        :colour "orange"
        :type "gate"
        :x 760
        :y 256
      }
      44 {
        :colour "orange"
        :type "gate"
        :x 768
        :y 256
      }
      45 {
        :colour "yellow"
        :type "gate"
        :x 816
        :y 264
      }
      46 {
        :colour "yellow"
        :type "gate"
        :x 824
        :y 264
      }
      48 {
        :colour "orange"
        :type "gate"
        :x 840
        :y 280
      }
      49 {
        :colour "orange"
        :type "gate"
        :x 840
        :y 272
      }
      50 {
        :colour "yellow"
        :type "gate"
        :x 832
        :y 264
      }
      51 {
        :colour "orange"
        :type "coin"
        :unique true
        :x 864
        :y 272
      }
      52 {
        :colour "pink"
        :heavy true
        :type "pad"
        :unique true
        :x 752
        :y 280
      }
      53 {
        :colour "red"
        :type "gate"
        :x 632
        :y 320
      }
    }
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 730.62858145595
        :y 366.83564983797
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 700.28957505367
        :y 370.08103955586
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}