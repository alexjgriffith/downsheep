(local pad {})

(fn pad.collide [pad]
  (tset pad :active true)
  pad)

(fn pad.update [pad dt]
  (when (and pad pad.active dt)
    (love.event.push :pad-up pad)
    ))

(fn pad.draw [pad camera]
  (when (and pad camera)
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    (local quad (if pad.active pad.down-quad pad.up-quad))
    (love.graphics.draw pad.image quad
                        (math.floor (* (+ camera.x pad.x) camera.scale))
                        (math.floor (* (+ camera.y pad.y) camera.scale))
                        0
                        camera.scale
                        camera.scale)))

(local pad-mt {:__index pad
                :update pad.update
                :collide pad.collide
                :draw pad.draw})

;; obj {:type pad :colour :yellow :x 10 :y 10}
(fn pad.new [colour heavy x y]
  ;; down = active
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos]
    (love.graphics.newQuad pos.x pos.y 8 8 256 256))
  (local quads
         (match colour
     :yellow (lume.map [{:x 64 :y 0} {:x 72 :y 0}  {:x 80 :y 0} {:x 88 :y 0}] newQuad)
     :pink (lume.map [{:x 64 :y 8} {:x 72 :y 8} {:x 80 :y 8} {:x 88 :y 8}] newQuad)
     :orange (lume.map [{:x 64 :y 16} {:x 72 :y 16} {:x 80 :y 16} {:x 88 :y 16}] newQuad)
     :blue (lume.map [{:x 64 :y 24} {:x 72 :y 24} {:x 80 :y 24} {:x 88 :y 24}] newQuad)
     :red (lume.map [{:x 64 :y 32} {:x 72 :y 32} {:x 80 :y 32} {:x 88 :y 32}] newQuad)
     :white (lume.map [{:x 64 :y 40} {:x 72 :y 40} {:x 80 :y 40} {:x 88 :y 40}] newQuad)))
  (local down-quad (. quads (if heavy 4 2)))
  (local up-quad (. quads (if heavy 3 1)))
  (setmetatable {: x : y : colour : down-quad : up-quad : image
                 :w 8 :h 8
                 :type :pad
                 :heavy heavy
                 :name (.. :pad " " colour)
                 :active false} pad-mt))

pad
