(global draw-canvas (love.graphics.newCanvas 1280 720))

(draw-canvas:setFilter :nearest :nearest)

(global offsets {:x 0 :y 0})

(global toggle-fullscreen
        (fn []
          (assets.sounds.click:play)
          (local (pw ph flags) (love.window.getMode))
          (local [w h] [params.screen-width params.screen-height])
          (local (dx dy) (love.window.getDesktopDimensions flags.display))
          (if flags.fullscreen
              (do (tset flags :fullscreen false)
                  (tset flags :centered true)
                  (tset flags :x (math.max 0 (/ (- dx 1280) 2)))
                  (tset flags :y (math.max 0 (/ (- dy 720) 2))))
              (tset flags :fullscreen true))
          ;; (tset flags :centered true)
          (love.window.setMode w h flags)
          (local (cw ch _flags) (love.window.getMode))
          (tset offsets :x (math.max 0 (math.floor (-  (/ cw 2) (/ w 2)))))
          (tset offsets :y (math.max 0 (math.floor (-  (/ ch 2) (/ h 2)))))))

(global is-fullscreen
        (fn []
          (local (_w _h flags) (love.window.getMode))
          flags.fullscreen))

(global screenshot
        (fn []
          (assets.sounds.click:play)
          (love.graphics.captureScreenshot
           (string.format "screenshot-%s.png" (os.time)))))


(var mute false)
(global toggle-sound
 (fn  []
   (if mute
       (do
         (assets.sounds.click:play)
         (do (love.audio.setVolume 1) (set mute false)))
       (do  (love.audio.setVolume 0) (set mute true)))))

(global is-mute (fn [] mute))
