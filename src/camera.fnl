(local camera {})

(fn camera.game [camera sheep-1 sheep-2 world]
  (let [x1 (+ sheep-1.x 8)
        y1 (+ sheep-1.y 8)
        x2 (+ sheep-2.x 8)
        y2 (+ sheep-2.y 8)
        dx (- x1 x2)
        dy (- y1 y2)
        x (/ (- x1 (/ dx 2)) 1)
        y (/ (- y1 (/ dy 2)) 1)
        left -20
        right (/ 1280 camera.scale)
        top 0
        bottom (/ 720 camera.scale)]
    ;; unit vector and distance
    (tset camera :x (- (/ 1280 camera.scale 2) x))
    (tset camera :y (+ 0 (- (/ 752 camera.scale 2) y)))

    (tset camera.top :x (- camera.x))
    (tset camera.top :y (+ (- camera.y) top))
    (tset camera.bottom :x (- camera.x))
    (tset camera.bottom :y (+ (- camera.y) bottom))
    (tset camera.left :x (+ (- camera.x) left))
    (tset camera.left :y (- camera.y))
    (tset camera.right :x (+ (- camera.x) right))
    (tset camera.right :y (- camera.y))

    (when (and world (world:hasItem camera.top))
      (world:update camera.top camera.top.x camera.top.y))
    (when (and world (world:hasItem camera.bottom))
      (world:update camera.bottom camera.bottom.x camera.bottom.y))
    (when (and world (world:hasItem camera.left))
      (world:update camera.left camera.left.x camera.left.y))
      (when (and world (world:hasItem camera.right))
        (world:update camera.right camera.right.x camera.right.y))))

(fn check-key-down [keys key]
  (if (-> keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn sign0 [x]
    (if (= x 0) 0
      (> x 0) 1
      (< x 0) -1))

(fn camera.editor [camera keys dt]
  (let [lr (- (check-key-down keys :right) (check-key-down keys :left))
        ud (- (check-key-down keys :down) (check-key-down keys :up))
        speed camera.speed
        x camera.x
        y camera.y
        max (if (and (~= 0 lr) (~= 0 ud)) speed.max-s2 speed.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed.decay speed.rate) lr)
                     (* dt (sign0 speed.x) (- speed.decay)))
                  (- max) max))
    (set speed.y (lume.clamp
                           (+ speed.y
                              (* dt (+ speed.decay speed.rate) ud)
                              (* dt (sign0 speed.y) (- speed.decay)))
                           (- max) max))
    (when (and (< (math.abs speed.x) speed.floor) (= 0 lr) )
      (set speed.x 0))
    (when (and (< (math.abs speed.y) speed.floor) (= 0 ud))
      (set speed.y 0))
    (tset camera :x (lume.clamp (- x speed.x) -1280 0))
    (tset camera :y (lume.clamp (- y speed.y) -1280 0))))

camera
