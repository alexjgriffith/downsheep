{
  :data {
    :ground {
      :data {
        85 [0 0 0 0 0 0 0 0 0]
        86 [0 0 0 1 1 1 0 0 0]
        87 [0 0 2 1 1 1 2 0 0]
        88 [0 0 2 1 1 1 2 0 0]
        89 [0 0 2 1 1 1 2 0 0]
        90 [0 0 2 1 1 1 2 0 0]
        91 [0 0 2 1 1 1 2 0 0]
        92 [0 0 0 2 2 2 0 0 0]
        93 [0 3 3 2 2 2 3 3 0]
        94 [0 3 2 2 2 2 2 3 0]
        95 [0 3 2 2 2 2 2 3 0]
        96 [0 3 2 2 2 2 2 3 0]
        97 [0 3 2 2 2 2 2 3 0]
        98 [0 3 3 2 2 2 3 3 0]
        99 [0 0 0 2 2 2 0 0 0]
        100 [0 0 2 4 4 4 2 0 0]
        101 [0 0 2 4 4 4 2 0 0]
        102 [0 0 2 4 4 4 2 0 0]
        103 [0 0 2 4 4 4 2 0 0]
        104 [0 0 0 4 4 4 0 0 0]
        105 [0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "start"
        2 "ground"
        3 "fence"
        4 "finish"
      }
      :off-y 40
      :range {
        :maxx 105
        :maxy 49
        :minx 85
        :miny 41
      }
      :type "dense"
    }
    :objects [{
        :colour "yellow"
        :type "coin"
        :unique true
        :x 752
        :y 352
      } {
        :colour "pink"
        :type "coin"
        :unique true
        :x 776
        :y 368
      } {
        :colour "orange"
        :type "coin"
        :unique true
        :x 752
        :y 368
      } {
        :colour "blue"
        :type "coin"
        :unique true
        :x 776
        :y 352
      } {
        :colour "red"
        :type "coin"
        :unique true
        :x 760
        :y 360
      } {
        :colour "white"
        :type "coin"
        :unique true
        :x 768
        :y 360
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 702.19972099212
        :y 357.11091554345
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 719.94000800268
        :y 357.88376874814
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}