(local game {})

(local state (require :state))

(local levels (require :levels))
(local col (require :prefab-col))
(local camera-update (require :camera))
(local editor (require :editor))
(local quips (require :quips))

(var has-drawn-ground false)

(local params (require :params))

(fn draw-ground [level x y]
  (when (or state.editor (not has-draw-ground))
    (love.graphics.clear)
    (love.graphics.setColor params.colours.background)
    (love.graphics.rectangle "fill" 0 0 1280 720)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.draw level.tileset-batch)
    ))

(local ui-font (_G.assets.fonts.avara 30))

(local ui-font-small (_G.assets.fonts.avara 25))

(local ui-font-large (_G.assets.fonts.avara 100))

(fn draw-ui [w h]
  (local params (require :params))
  (local y 40)
  (love.graphics.setColor params.colours.white)
  ;; (love.graphics.rectangle "fill" 0 y w 4)
  (love.graphics.setColor params.colours.grey)
  (love.graphics.rectangle "fill" 0 (- y 40) w 40)
    (love.graphics.setColor params.colours.white)
  (love.graphics.rectangle (if (> state.flash-ui 0) "fill" "line") 0 (- y 40) w 40)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setFont ui-font)
  (if (and (~= "" (levels.hint state.current-level)) (> (levels.current-time state.current-level) 30))
    ;; (pp "when")
      (do (love.graphics.printf (levels.display state.current-level) 120 (- y 35 ) 170)
    (love.graphics.setFont ui-font-small)
    (love.graphics.printf (levels.hint state.current-level) 290 (- y 34 ) 700 :center))
    (love.graphics.printf (levels.display state.current-level) 555 (- y 35 ) 170 :center))
  (each [_ icon (pairs state.icons)] (icon:draw)))

(fn update-pointer []
  (local camera state.camera)
  (local pointer state.pointer)
  (local (x y) (love.mouse.getPosition))
  (tset pointer :i  (math.floor (/ (- (/ x 4) (* 1 (+ (/ offsets.x 4) camera.x))) (* 1 8))))
  (tset pointer :j  (math.floor (/ (- (/ y 4) (* 1 (+ (/ offsets.y 4) camera.y))) (* 1 8)))))

(fn draw-game [self]
    (local {: level : canvas : quad : world : camera : sheep-1 : sheep-2} state)
  (local (w h flags) (love.window.getMode))
  (love.graphics.setCanvas canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1 1)
  (draw-ground level)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.clear)
  (quad:setViewport  (- camera.x) (- camera.y) 320 180 1280 720)
  ;; (love.graphics.rectangle "fill" 0 0 1280 720)
  ;; (love.graphics.clear)
  (love.graphics.draw canvas quad  0 0 0 camera.scale)
  (each [_ object (pairs state.objects)] (object:draw camera))
  (when (not state.menu)
    (draw-ui w h)
    )
  (if (> sheep-2.y sheep-1.y)
      (do (sheep-1:draw camera)
          (sheep-2:draw camera))
      (do (sheep-2:draw camera)
          (sheep-1:draw camera)))
  (when (not state.editor) (quips.draw))
  (when (and (not state.menu) state.editor)
    (editor.draw state))
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.clear)
  (love.graphics.draw draw-canvas offsets.x offsets.y)
  )

(fn game.draw [self]
  ;; (love.graphics.setColor 1 1 1 1)
  (draw-game self)
  ;; (love.graphics.setFont ui-font-large)
  ;; (love.graphics.printf "DOWN SHEEP" 200 300 1080 :center)
  ;; (when state.menu
  ;;   (love.graphics.setColor 1 1 1 0.5)
  ;;   (love.graphics.rectangle "fill" 0 0 1280 720)
  ;;   (love.graphics.setColor 1 1 1 1))
  ;; (col.draw state.world state.camera)
  )

(fn game.update [self dt]
  (local {: world : keys : objects : sheep-1 : sheep-2} state)
  (levels.current-time-inc state.current-level dt)
  (if state.editor
      (camera-update.editor state.camera state.keys dt)
      (camera-update.game state.camera sheep-1 sheep-2 world))
  (each [_ object (pairs state.objects)] (when object (object:update dt)))
  (when (and (not state.editor) (not state.menu))
    (quips.update sheep-1 sheep-2 state.camera dt)
    (state.sheep-1:update world keys dt)
    (state.sheep-2:update world keys dt))
  (update-pointer)
  (tset state :flash-ui (- state.flash-ui dt))
  (when state.editor
    (editor.update dt))
  )

(fn game.leave [self ...]
  (love.mouse.setVisible true))

(fn game.enter [_self _from reset?]
  (love.mouse.setVisible false)
  (tset state :loaded true)
  (set has-drawn-ground false)
  (match reset?
    :first (levels.load state :data-tutorial-1)
    :reset (levels.load state state.current-level)
    :new-level (levels.load state nil)
    :resume nil
    _ (levels.load state state.current-level)))

(fn game.mousereleased [self x y button]
  (when state.editor (editor.mousereleased self x y button state)))

(fn game.mousepressed [self x y button]
  (when state.editor (editor.mousepressed self x y button state)))

(fn game.keypressed [self key code]
  (local {: world} state)
  (match key
    :r (do
         (local sheep-1 (. state.level.map.data.players "sheep one"))
         (local sheep-2 (. state.level.map.data.players "sheep two"))
         (state.world:update state.sheep-1 sheep-1.x sheep-1.y)
         (state.world:update state.sheep-1 sheep-1.x sheep-1.y)
         (state.world:update state.sheep-2 sheep-2.x sheep-2.y))
    :n (do
         (if state.sheep-1.active
             (do (tset state.sheep-1 :active false)
                 (tset state.sheep-2 :active true))
             (do (tset state.sheep-1 :active true)
                 (tset state.sheep-2 :active false))
             ))
    :return (when (not _G.web)
               (if state.editor
                (do (tset state :editor false)
                    (love.mouse.setVisible false))
                (do (tset state :editor true)
                    (love.mouse.setVisible true))))
    :escape (do
              (local gamestate (require :lib.gamestate))
              (assets.sounds.page:play)
              (gamestate.switch (require "mode-menu")))
    :f10 (when (not _G.web) (toggle-fullscreen))
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))
    _ (when state.editor (editor.keypressed self key code state))
    ))

game
