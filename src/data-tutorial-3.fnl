{
  :data {
    :ground {
      :data {
        84 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        85 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        86 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        87 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        88 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        89 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        90 [0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0]
        91 [0 0 0 0 0 0 0 0 1 1 1 0 2 2 2 0]
        92 [0 1 3 3 3 1 0 1 1 1 1 1 4 4 2 0]
        93 [0 1 3 3 3 1 1 1 1 1 1 1 4 4 2 0]
        94 [0 1 3 3 3 1 1 1 1 1 1 1 4 4 2 0]
        95 [0 1 3 3 3 1 0 1 1 1 1 1 4 4 2 0]
        96 [0 0 3 3 3 0 0 0 1 1 1 0 2 2 2 0]
        97 [0 0 3 3 3 0 0 0 1 1 1 0 0 0 0 0]
        98 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "ground"
        2 "fence"
        3 "start"
        4 "finish"
      }
      :off-y 37
      :range {
        :maxx 98
        :maxy 53
        :minx 84
        :miny 38
      }
      :type "dense"
    }
    :objects [{
        :colour "yellow"
        :type "gate"
        :x 744
        :y 352
      } {
        :colour "yellow"
        :type "gate"
        :x 752
        :y 352
      } {
        :colour "yellow"
        :heavy true
        :type "pad"
        :unique true
        :x 768
        :y 376
      } {
        :item "weight"
        :type "decal"
        :unique true
        :x 680
        :y 368
      } {
        :colour "pink"
        :type "pad"
        :unique true
        :x 768
        :y 328
      } {
        :colour "pink"
        :type "gate"
        :x 720
        :y 368
      } {
        :colour "pink"
        :type "gate"
        :x 720
        :y 376
      } {
        :colour "pink"
        :type "gate"
        :x 720
        :y 384
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 743.08217475461
        :y 323.45044678369
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 741.17542608385
        :y 368.65704336486
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}