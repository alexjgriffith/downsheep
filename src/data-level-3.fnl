{
  :data {
    :ground {
      :data {
        74 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        75 [0 0 0 0 0 0 1 1 0 0 0 0 1 1 1 0 0 0 0 0 0]
        76 [0 0 1 1 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 0]
        77 [0 0 1 1 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 0]
        78 [0 0 1 1 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 0]
        79 [0 0 1 1 0 0 1 1 0 0 0 1 1 1 1 0 0 0 0 0 0]
        80 [0 0 1 1 1 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0]
        81 [0 0 0 1 1 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        82 [0 0 0 1 1 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        83 [0 1 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        84 [0 0 1 1 1 1 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        85 [0 2 1 1 1 1 1 0 0 0 0 1 1 1 1 0 0 1 1 1 0]
        86 [0 2 1 1 1 1 1 0 0 0 0 1 1 1 1 0 0 1 1 1 0]
        87 [0 2 1 1 1 1 1 0 0 0 0 1 1 1 1 0 0 1 1 1 0]
        88 [0 2 1 3 3 3 1 0 0 0 0 1 1 1 1 1 0 1 0 1 0]
        89 [0 2 1 3 3 3 1 1 1 1 1 1 1 1 1 1 1 1 0 1 0]
        90 [0 2 1 3 3 3 1 1 1 1 1 1 1 1 1 1 1 1 0 1 0]
        91 [0 2 1 3 3 3 1 0 0 0 0 1 4 4 4 1 1 1 0 1 0]
        92 [0 2 1 3 3 3 1 0 0 0 0 1 4 4 4 1 0 1 0 1 0]
        93 [0 2 1 1 1 1 1 0 0 0 0 1 4 4 4 0 0 1 1 1 0]
        94 [0 2 1 1 1 1 1 0 0 0 0 1 4 4 4 0 0 1 1 1 0]
        95 [0 2 1 1 1 1 1 0 0 0 0 1 1 1 1 0 0 1 1 1 0]
        96 [0 0 1 1 1 1 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        97 [0 1 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        98 [0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        99 [0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        100 [0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0]
        101 [0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0]
        102 [0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0]
        103 [0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0]
        104 [0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0]
        105 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "ground"
        2 "fence"
        3 "finish"
        4 "start"
      }
      :off-y 32
      :range {
        :maxx 105
        :maxy 53
        :minx 74
        :miny 33
      }
      :type "dense"
    }
    :objects [{
        :colour "yellow"
        :type "coin"
        :unique true
        :x 688
        :y 296
      } {
        :colour "pink"
        :type "coin"
        :unique true
        :x 752
        :y 288
      } {
        :colour "blue"
        :type "pad"
        :unique true
        :x 720
        :y 312
      } {
        :type "tree"
        :x 704
        :y 400
      } {
        :type "tree"
        :x 712
        :y 400
      } {
        :type "tree"
        :x 728
        :y 400
      } {
        :type "tree"
        :x 736
        :y 400
      } {
        :colour "blue"
        :type "coin"
        :unique true
        :x 688
        :y 360
      } {
        :colour "red"
        :type "coin"
        :unique true
        :x 712
        :y 376
      } {
        :colour "blue"
        :type "gate"
        :x 712
        :y 336
      } {
        :colour "white"
        :type "gate"
        :x 720
        :y 336
      } {
        :colour "yellow"
        :type "gate"
        :x 776
        :y 360
      } {
        :colour "yellow"
        :type "gate"
        :x 776
        :y 368
      } {
        :colour "pink"
        :type "gate"
        :x 664
        :y 368
      } {
        :colour "pink"
        :type "gate"
        :x 664
        :y 360
      } {
        :colour "pink"
        :type "pad"
        :unique true
        :x 712
        :y 368
      } {
        :colour "yellow"
        :heavy true
        :type "pad"
        :unique true
        :x 600
        :y 368
      } {
        :item "axe"
        :type "decal"
        :unique true
        :x 616
        :y 352
      } {
        :item "weight"
        :type "decal"
        :unique true
        :x 680
        :y 400
      } {
        :item "feather"
        :type "decal"
        :unique true
        :x 808
        :y 352
      } {
        :colour "white"
        :type "pad"
        :unique true
        :x 616
        :y 320
      } {
        :colour "orange"
        :type "coin"
        :unique true
        :x 600
        :y 312
      } {
        :colour "white"
        :type "coin"
        :unique true
        :x 752
        :y 400
      } {
        :colour "blue"
        :type "gate"
        :x 640
        :y 288
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 747.17321920792
        :y 358.02048415627
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 729.56009453793
        :y 356.23152572179
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}