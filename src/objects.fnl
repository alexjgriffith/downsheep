(local objects {})

(var gate-num 0)
(var tree-num 0)

(fn objects.gen-objects [objs]
  (local coin (require :prefab-coin))
  (local pad (require :prefab-pad))
  (local gate (require :prefab-gate))
  (local decal (require :prefab-decal))
  (local tree (require :prefab-tree))
  (local ret {})
  (each [_ object (pairs objs)]
    (match object.type
      :coin (tset ret (.. :coin " " object.colour)
                  (coin.new object.colour object.x object.y))
      :pad (tset ret (.. :pad " " object.colour)
                 (pad.new object.colour object.heavy object.x object.y))
      :gate (do (tset ret (.. :gate " " object.colour " " gate-num)
                      (gate.new object.colour gate-num object.x object.y))
                (set gate-num (+ gate-num 1)))
      :decal (tset ret (.. :decal " " object.item)
                   (decal.new object.item object.x object.y))
      :tree (do (tset ret (.. :tree " " tree-num) (tree.new tree-num object.x object.y))
                (set tree-num (+ tree-num 1)))
      ))
  ret
  )

(fn objects.gen-object [object]
  (var ret {})
  (each [name value (pairs (objects.gen-objects [object]))]
    (set ret value))
  ret)

(fn sort-objects [a b]
  (let [va (or (or a.name (or a.item a.colour)) "z")
        vb (or (or a.name (or b.item b.colour)) "z")]
    (< va va)))

(fn objects.gen-icons [objs]
  (local (_w h _flags) (love.window.getMode))
  (local w 1280)
  (local icon (require :prefab-icon))
  (local ret {})
  (local y 4);;(- h 32 4))
  (var x-decal 4)
  (var x-coin (- w 32 4))
  (each [_ object (pairs objs)]
    (match object.type
      :coin (do (tset ret (.. :coin " " object.colour)
                      (icon.new object.colour x-coin y))
                (set x-coin (- x-coin 4 32)))
      :decal (do (tset ret (.. :decal " " object.item)
                       (icon.new object.item x-decal y))
                 (set x-decal (+ x-decal 4 32)))
      ))
  ret
  )

objects
