{
  :data {
    :ground {
      :data {
        84 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]
        85 [1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1]
        86 [1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1]
        87 [1 0 0 0 0 0 3 3 3 0 0 0 1 1 1 1 1 1 1 1 1]
        88 [1 0 0 0 0 0 3 3 3 0 0 0 1 1 1 0 0 0 0 2 1]
        89 [1 0 0 0 0 0 3 3 3 0 0 0 0 1 0 0 0 0 0 2 1]
        90 [1 0 0 0 0 0 3 3 3 0 0 0 0 1 0 0 0 0 0 2 1]
        91 [1 0 0 0 0 0 3 3 3 0 0 0 0 1 0 0 0 0 0 2 1]
        92 [1 0 0 0 0 0 3 3 3 0 0 0 1 1 1 0 0 0 0 2 1]
        93 [1 0 0 0 0 0 3 3 3 0 0 0 1 1 1 1 1 1 1 1 1]
        94 [1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1]
        95 [1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1]
        96 [1 1 1 1 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 1]
        97 [1 1 1 1 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 1]
        98 [1 1 1 1 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 1]
        99 [1 0 0 0 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 1]
        100 [1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1]
        101 [1 0 0 0 2 2 1 1 1 1 2 2 1 1 1 1 1 1 1 1 1]
        102 [1 1 1 1 2 4 4 4 4 4 4 2 1 1 1 1 1 1 1 1 1]
        103 [1 1 1 1 2 4 4 4 4 4 4 2 1 1 1 1 1 1 1 1 1]
        104 [1 1 1 1 2 4 4 4 4 4 4 2 1 1 1 1 1 1 1 1 1]
        105 [1 1 1 1 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1]
        106 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]
      }
      :map {
        0 "ground"
        1 "chasm"
        2 "fence"
        3 "start"
        4 "finish"
      }
      :off-y 38
      :range {
        :maxx 106
        :maxy 59
        :minx 84
        :miny 39
      }
      :type "dense"
    }
    :objects [{
        :colour "yellow"
        :type "gate"
        :x 768
        :y 360
      } {
        :colour "yellow"
        :type "gate"
        :x 768
        :y 352
      } {
        :colour "yellow"
        :type "pad"
        :unique true
        :x 728
        :y 336
      } {
        :type "tree"
        :x 768
        :y 392
      } {
        :type "tree"
        :x 768
        :y 400
      } {
        :item "weight"
        :type "decal"
        :unique true
        :x 712
        :y 440
      } {
        :item "feather"
        :type "decal"
        :unique true
        :x 792
        :y 320
      } {
        :item "axe"
        :type "decal"
        :unique true
        :x 712
        :y 440
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 703.29285221612
        :y 349.64398128777
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 728.5342496307
        :y 352.34681767535
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}