{
  :data {
    :ground {
      :data {
        83 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        84 [0 0 0 1 1 1 1 1 1 0 0 0 0 0 0 0]
        85 [0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0]
        86 [0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0]
        87 [0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0]
        88 [0 0 0 1 1 1 1 1 1 0 0 0 0 0 0 0]
        89 [0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0]
        90 [0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0]
        91 [0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0]
        92 [0 0 0 1 1 1 1 1 1 0 1 1 1 1 1 0]
        93 [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 0]
        94 [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 0]
        95 [0 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0]
        96 [0 2 2 2 2 1 1 1 1 0 1 1 1 1 1 0]
        97 [0 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0]
        98 [0 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0]
        99 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "ground"
        2 "finish"
      }
      :off-y 40
      :range {
        :maxx 99
        :maxy 56
        :minx 83
        :miny 41
      }
      :type "dense"
    }
    :objects [{
        :type "tree"
        :x 720
        :y 376
      } {
        :type "tree"
        :x 720
        :y 368
      } {
        :colour "yellow"
        :type "gate"
        :x 744
        :y 400
      } {
        :colour "yellow"
        :type "gate"
        :x 752
        :y 400
      } {
        :colour "yellow"
        :type "gate"
        :x 760
        :y 400
      } {
        :colour "yellow"
        :type "pad"
        :unique true
        :x 688
        :y 400
      } {
        :item "axe"
        :type "decal"
        :unique true
        :x 744
        :y 416
      }]
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 697.95468198604
        :y 362.05076889994
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 736
        :y 360
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}