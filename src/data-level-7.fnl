{
  :data {
    :ground {
      :data {
        81 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        82 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 0 0 2 2 2 0 0]
        83 [0 0 2 2 2 0 0 0 0 0 0 0 0 2 2 2 2 2 2 4 2 2 2 2 0]
        84 [0 2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 2 2 2 4 2 2 2 2 0]
        85 [0 2 2 2 2 2 2 0 0 0 0 0 0 0 2 2 2 2 2 4 2 2 2 2 0]
        86 [0 2 2 2 2 2 0 0 0 0 0 0 0 0 0 2 2 2 0 4 2 2 2 0 0]
        87 [0 0 2 2 2 0 0 2 2 2 2 2 2 2 0 0 2 2 0 0 2 2 0 0 0]
        88 [0 0 2 2 2 0 0 2 2 3 3 3 2 2 0 0 2 2 2 0 2 2 0 0 0]
        89 [0 0 2 2 2 2 2 2 2 3 3 3 2 2 2 2 2 2 2 2 2 2 0 0 0]
        90 [0 0 2 2 2 2 2 2 2 3 3 3 2 2 2 2 2 2 2 2 2 2 0 0 0]
        91 [0 0 2 2 2 2 2 2 2 3 3 3 2 2 2 2 2 2 0 0 0 2 2 0 0]
        92 [0 0 2 2 2 0 0 2 2 3 3 3 2 2 0 0 2 2 2 0 2 2 2 2 0]
        93 [0 0 0 0 0 0 0 2 2 3 3 3 2 2 0 0 2 2 2 0 2 2 2 2 0]
        94 [0 0 0 0 0 0 0 2 2 2 2 2 2 2 0 0 2 2 0 0 0 2 2 2 0]
        95 [0 0 0 0 0 0 0 2 2 2 2 2 2 2 0 0 2 2 0 0 0 0 2 2 0]
        96 [0 0 0 0 0 0 0 4 4 4 4 4 0 0 0 0 2 2 0 0 0 0 2 2 0]
        97 [0 0 0 0 0 0 0 4 1 1 1 1 0 0 0 0 2 2 0 0 0 0 2 2 0]
        98 [0 0 0 0 0 2 2 2 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 0]
        99 [0 0 0 0 0 2 2 2 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 0]
        100 [0 0 0 2 2 2 2 2 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 0]
        101 [0 0 2 2 2 2 0 4 1 1 1 1 0 0 0 0 2 2 0 0 0 0 0 0 0]
        102 [0 0 2 2 2 2 0 4 4 4 4 4 0 0 0 0 2 2 0 0 0 0 0 0 0]
        103 [0 0 2 2 2 2 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0]
        104 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0]
        105 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 0]
        106 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0 0 0]
        107 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0 0 0]
        108 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0 0 0]
        109 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 2 0 0 0 0 0]
        110 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "finish"
        2 "ground"
        3 "start"
        4 "fence"
      }
      :off-y 30
      :range {
        :maxx 110
        :maxy 55
        :minx 81
        :miny 31
      }
      :type "dense"
    }
    :objects {
      4 {
        :colour "yellow"
        :type "pad"
        :unique true
        :x 720
        :y 272
      }
      8 {
        :colour "yellow"
        :type "gate"
        :x 728
        :y 360
      }
      9 {
        :colour "yellow"
        :type "gate"
        :x 712
        :y 360
      }
      10 {
        :colour "pink"
        :type "gate"
        :x 720
        :y 360
      }
      11 {
        :type "tree"
        :x 696
        :y 280
      }
      12 {
        :type "tree"
        :x 696
        :y 272
      }
      14 {
        :colour "white"
        :heavy true
        :type "pad"
        :unique true
        :x 792
        :y 392
      }
      15 {
        :type "tree"
        :x 760
        :y 376
      }
      16 {
        :type "tree"
        :x 760
        :y 384
      }
      17 {
        :type "tree"
        :x 696
        :y 264
      }
      19 {
        :colour "orange"
        :type "gate"
        :x 696
        :y 376
      }
      20 {
        :colour "orange"
        :type "gate"
        :x 696
        :y 384
      }
      21 {
        :colour "pink"
        :type "pad"
        :unique true
        :x 720
        :y 384
      }
      22 {
        :item "axe"
        :type "decal"
        :unique true
        :x 664
        :y 368
      }
      24 {
        :colour "pink"
        :type "gate"
        :x 712
        :y 400
      }
      25 {
        :colour "pink"
        :type "gate"
        :x 720
        :y 400
      }
      26 {
        :colour "orange"
        :type "pad"
        :unique true
        :x 744
        :y 424
      }
      27 {
        :colour "blue"
        :type "pad"
        :unique true
        :x 792
        :y 424
      }
      28 {
        :colour "blue"
        :type "gate"
        :x 824
        :y 384
      }
      29 {
        :colour "blue"
        :type "gate"
        :x 824
        :y 376
      }
      30 {
        :colour "white"
        :type "gate"
        :x 760
        :y 424
      }
      31 {
        :colour "white"
        :type "gate"
        :x 760
        :y 432
      }
      32 {
        :colour "orange"
        :type "gate"
        :x 696
        :y 416
      }
      33 {
        :colour "orange"
        :type "gate"
        :x 696
        :y 408
      }
      34 {
        :item "feather"
        :type "decal"
        :unique true
        :x 672
        :y 264
      }
      35 {
        :item "weight"
        :type "decal"
        :unique true
        :x 664
        :y 408
      }
      36 {
        :colour "white"
        :type "gate"
        :x 800
        :y 408
      }
      37 {
        :colour "white"
        :type "gate"
        :x 792
        :y 408
      }
      38 {
        :colour "white"
        :type "gate"
        :x 784
        :y 408
      }
      39 {
        :colour "pink"
        :type "coin"
        :unique true
        :x 848
        :y 360
      }
      40 {
        :colour "orange"
        :type "coin"
        :unique true
        :x 872
        :y 392
      }
      41 {
        :colour "blue"
        :type "coin"
        :unique true
        :x 872
        :y 360
      }
      42 {
        :colour "red"
        :type "coin"
        :unique true
        :x 848
        :y 392
      }
      43 {
        :colour "white"
        :type "coin"
        :unique true
        :x 864
        :y 376
      }
      48 {
        :colour "red"
        :type "pad"
        :unique true
        :x 816
        :y 272
      }
      49 {
        :colour "white"
        :type "gate"
        :x 784
        :y 360
      }
      50 {
        :colour "white"
        :type "gate"
        :x 800
        :y 360
      }
      51 {
        :colour "red"
        :type "gate"
        :x 792
        :y 360
      }
      52 {
        :colour "yellow"
        :type "coin"
        :unique true
        :x 856
        :y 376
      }
    }
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 708.02458777494
        :y 319.42851446994
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 734.01912744367
        :y 321.50207532715
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}