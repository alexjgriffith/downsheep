(local map (require :lib.map))

(fn addremove [type mapin x y delete? hover? tileset?]
  (local layer (or hover? (. mapin.tile-set type :layer)))
  (local quad (. mapin.tile-set type :quad))
  (if (not delete?)
      (map.add-tile mapin x y layer {:type type :quad quad})
      (map.remove mapin x y layer)))

(local editor {})

(fn editor.update [self layer]
  (local {: tileset-batch  :map mapin} self)
  (map.update-tileset-batch tileset-batch mapin layer))

(fn editor.load-map [_self map-file? from-clipboard? revert?]
  (if from-clipboard?
      (do
        (local map-file (fennel.eval (love.system.getClipboardText)))
        (map.load map-file))
      revert?
      (map.load (require map-file?))
      map-file?
      (let [map-file (if (and (love.filesystem.isFused)
                              (love.filesystem.exists (.. map-file? ".fnl")))
                         (do (pp (.. "Game is Fused: Loading from " map-file? ".fnl"))
                             (fennel.eval (love.filesystem.read (.. map-file? ".fnl")) {}))
            (require map-file?))]
        (map.load map-file))
      (do (map.new))))

(fn editor.add-tile [self x y interactive?]
  (let [{:map mapin
         : tileset-batch
         : brush
         } self]
    (if (= :string (type brush))
      (do (local (tile replace) (addremove brush mapin x y))
          (when interactive?
            (map.auto-index mapin :ground)
            (map.update-tileset-batch tileset-batch mapin :ground)
            (if replace
                (love.event.push :edit-map :replace-tile tile {:w 8 :h 8} replace)
                (love.event.push :edit-map :add-tile tile {:w 8 :h 8})))
          (values tile replace))
      (editor.add-object self brush x y))))

(fn editor.add-tile-region [self start-x end-x start-y end-y]
    (for [i start-x end-x]
      (for [j start-y end-y]
        (local (tile replace) (editor.add-tile self i j))
        (if replace
            (love.event.push :edit-map :replace-tile tile {:w 8 :h 8} replace)
            (love.event.push :edit-map :add-tile tile {:w 8 :h 8}))))
    (map.auto-index self.map :ground)
    (map.update-tileset-batch self.tileset-batch self.map :ground))

(fn editor.remove-tile [self brush x y]
    (let [{:map mapin
         : tileset-batch
         : tile-layers
         : brushes} self]
    (local tile (addremove brush mapin x y :delete))
    (when interactive?
      (map.auto-index mapin :ground)
      (map.update-tileset-batch tileset-batch mapin :ground)
      (love.event.push :edit-map  :remove-tile tile {:w 8 :h 8}))))

(fn editor.add-object [self obj x y]
  (var object-index (+ 1 (# self.map.data.objects)))
  (when obj.unique
    (each [index value (pairs self.map.data.objects)]
      (when (and (= value.colour obj.colour)
                 (= value.item obj.item)
                 (= value.type obj.type))
        (set object-index index)))
    )
  (local ret (lume.clone obj))
  (tset ret :x (* x 8))
  (tset ret :y (* y 8))
  ;; (editor.remove-object  self x y)
  (tset self.map.data.objects object-index ret)
  (love.event.push :edit-obj :add-object ret)
  (values ret nil))

(fn editor.remove-object [self x y]
  (each [index object (pairs self.map.data.objects)]
    (when (and (and (>= x object.x) (< x (+ (or object.w 8) object.x)))
               (and (>= y object.y) (< y (+ (or object.h 8) object.y))))
      (love.event.push :edit-obj :remove-object object)
      (tset self.map.data.objects index nil)
      )))

(fn editor.hover [self brush x y]
  (let [{: hover-batch} self]
    (local hovermap {:tile-set self.map.tile-set
                     :width self.map.width
                     :height self.map.height
                     :id 0
                     :tile-size 8
                     :data {:hover {}}})
    (local tile (addremove brush hovermap 0 0 nil :hover))
    (map.auto-index hovermap :hover)
    (map.update-tileset-batch hover-batch hovermap :hover)))

(fn editor.save [self mapin file copy-to-clipboard]
  (map.clear-quads mapin.tile-set)
  (map.to-dense mapin :ground)
  (let [f (assert (io.open file "wb"))
        c (: f :write ((require "lib.fennelview") mapin))]
    (: f :close)
    c)
  (when copy-to-clipboard
    (local p (require :lib.fennelview))
    (local m (p mapin))
    (love.system.setClipboardText (m:gsub "%] %[" "%]\n%[")))
  (map.load mapin)
  nil)

(local editor-mt {
                  :__index editor
                  :update editor.update
                  :load-level editor.load-level
                  :add-tile editor.add-tile
                  :add-tile-region editor.add-tile-region
                  :remove-tile editor.remove-tile
                  :add-object editor.add-object
                  :remove-object editor.remove-object
                  :hover editor.hover
                  :save editor.save})


(fn [map-file tile-sheet from-clipboard? revert? options?]
  (local mapin (editor.load-map nil map-file from-clipboard? revert?);; (if
               ;;  from-clipboard? (map.load (require map-file)
               ;;  map-file       (map.load (require map-file))
               ;;     (map.new))
                )
  (fn tile-to-pixel [px tile-size]
    (* px tile-size))
  (local default
         {:tile-layers [:ground]
          :object-layers [:objs]
          :all-layers [:ground :objs]
          })
  (each [key value (pairs (or options? {}))]
    (tset default key value))
  (local tileset-batch (love.graphics.newSpriteBatch
                        tile-sheet (* 80 45 2)))
  (local hover-batch (love.graphics.newSpriteBatch
                      tile-sheet 20))
  (local sample-grid
         (map.newGrid 4 4 256 256))
  (local level
         (setmetatable
          {:tile-sheet tile-sheet
           :sample-grid sample-grid
           :map mapin
           :tile-size mapin.tile-size
           :map-width mapin.width
           :map-height mapin.height
           :tileset-batch tileset-batch
           :hover-batch hover-batch
           :level nil
           :object-layers default.object-layers
           :tile-layers default.tile-layers
           :all-layers default.all-layers
           :render true
           :brush :fence
           } editor-mt))
  (editor.update level :ground)
  level)
