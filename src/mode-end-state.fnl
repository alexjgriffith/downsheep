(local end-state {})

(var timer 0)

(local state (require :state))

(var step :leave)

(local levels (require :levels))

(var alpha 0)

(var alpha-2 0)

(local gamestate (require :lib.gamestate))

(local buttons (require :lib.buttons))

(local params (require :params))

(local mode-game (require :mode-game))

(local credit-text
       "
Game by Alexander Griffith

Music (Thunder Hoodie Beats - Isolated) - (CCBY 4.0)
Font (Avara) - (OFL)
Font (Inconsolata) - (OFL)
Library (Lume) - RXI (MIT/X11)
Library (anim8,bump,gamera) - Enrique Cota (MIT)
Engine (LÖVE) - LÖVE Dev Team (Zlib)
Language (Fennel) - Calvin Rose (MIT/X11)")

(var enough-flowers false)

(var no-flowers false)

(local elements
       [{:type :title :y 10 :w 400 :h 60 :text "CREDITS"}
        {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Play Again" }
        {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text credit-text}])

(local element-font
       {:title  ((. assets.fonts "avara") 70)
        :button  ((. assets.fonts "avara") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})


(fn play-again []
  (local mode-game (require :mode-game))
  (set timer 0)
  (set step :leave)
  (assets.sounds.page:play)
  (mode-game.enter nil nil :first)
  (gamestate.switch (require "mode-menu") :end-game))

(local element-click
       {"Play Again"
        play-again})

(local element-hover {:button (fn [element x y] :nothing)})

(local ui (buttons elements params element-click element-hover element-font))

(fn end-state.draw [self]
  (mode-game.draw self)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.clear)
  (love.graphics.setColor (/ 34 256) (/ 32 256) (/ 52 256) (lume.clamp alpha 0 1))
  (love.graphics.rectangle "fill" 0 0 1280 780)
  (love.graphics.setColor 1 1 1 1)
  (when (= :load step)
    (love.graphics.setFont ((. assets.fonts "avara") 70))
    (if enough-flowers
        (love.graphics.printf ["Here are your flowers Gregory\n\nIt's not so bad being a sheep"] 20 250 1240 :center)
        no-flowers
        (love.graphics.printf ["Oh god wake up Gregory!\n\nI swear I will collect more flowers!"] 20 250 1240 :center)
        (love.graphics.printf ["Did we pick enough flowers?\n\nI don't think so Allen *caugh*"]  20 250 1240 :center))
    )
  (love.graphics.setColor 1 1 1 (lume.clamp alpha-2 0 1))
  (love.graphics.rectangle "fill" 0 0 1280 780)
  ;; (love.graphics.setColor 1 1 1 1)
  (when (and (~= step :leave ) (~= step :load )) (ui:draw))
  ;; (when (= :enter step) (ui:draw))
  ;; (when (= :done step) (ui:draw))
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw draw-canvas offsets.x offsets.y)
  )

(fn end-state.update [self dt]
  ;; (mode-game.update self dt)
  (local flowers-collected (levels.count-flowers))
  (set enough-flowers (> flowers-collected 60))
  (set no-flowers (< flowers-collected 30))
  (ui:update dt)
  ;; (when (and (~= step :leave ) (~= step :load )) (ui:update dt))
  ;;(when (= :done step) (ui:update dt))
  (match step
    :leave
    (do
      (if (> timer 1.5)
          (do
            (set step :load)
            (set timer 0)
            (set alpha 1))
          (do
            (set timer (+ timer dt))
            (set alpha timer)))
      )
    :load
    (do
      ;; (tset state :current-level :data-level-1)
      (if (> timer 5)
          (do (set step :enter)
              (set timer 0))
          (set timer (+ timer dt))))
    :enter
    (do
      (if (> timer 1.5)
          (do
            (set step :done)
            (set timer 0)
            (set alpha-2 1))
          (do
            (set timer (+ timer dt))
            (set alpha-2 timer)))
      )
    :done (do
            (love.mouse.setVisible true))
    )
  )

(fn end-state.enter [self from ...]
  (love.mouse.setVisible false)
  (set timer 0)
  (set alpha 0)
  (set alpha-2 0)
  (set step :leave)
  (tset state :menu true))

(fn end-state.leave [self from ...]
  (tset state :menu false))

(fn end-state.mousereleased [self ...]
  (when (= :done step)
    (ui:mousereleased ...)))

(fn end-state.keypressed [self key code]
  (match key
    :f10 (when (not _G.web) (toggle-fullscreen))
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))
    "escape" (play-again)
    ))

end-state
