(local col {})

(local bump (require :lib.bump))

;; ((require :prefab-cols) level.map [] [])
;; draw
(fn col.draw [world camera]
  (love.graphics.push "all")
  ;; (love.graphics.setCanvas)
  ;;(love.graphics.setColor 1 1 1 0.5)
  (local (items len) (world:getItems))
  (love.graphics.setColor 1 1 1 1)
  (each [key value (pairs items)]
    (when (or (= :sheep value.type) (= :fence value.type)
              (= :coin value.type) (= :pad value.type)
              (= :gate value.type) (= :decal value.type)
              (= :camera value.type)
              )
      (local (x y w h) (world:getRect value))
      (love.graphics.rectangle "line" (* camera.scale (+ x camera.x)) (* camera.scale (+ y camera.y)) (* camera.scale w) (* camera.scale h))))
  (love.graphics.pop))

;; move player
;; (fn col.move [world x y item]
;;   (local (ax ay cols len) (world:move item x y))
;;   (values ax ay cols len))

;; add tile
(fn col.add-tile [world mapin tile]
  (local collidable (. mapin.tile-set tile.type :collidable))
  (local [x y] [(* 8 tile.x) (* 8 tile.y)])
  (when (~= tile.type :ground) (world:add tile x y 8 8)))

;; remove tile
(fn col.remove-tile [world]
  (world:remove tile))

;; create map
(fn col.new [mapin players objects camera]
  (local world (bump.newWorld 32))
  (local tiles mapin.data.ground)
  (each [id tile (pairs tiles)]
    (col.add-tile world mapin tile))
  (each [id player (pairs players)]
    (world:add player (+ player.x 6) (+ player.y 12) 4 4))
  (each [id object (pairs objects)]
    (world:add object object.x object.y object.w object.h object.collision-type))
  (local {: top : bottom : right : left} camera)
  (world:add top top.x top.y top.w top.h)
  (world:add bottom bottom.x bottom.y bottom.w bottom.h)
  (world:add right right.x right.y right.w right.h)
  (world:add left left.x left.y left.w left.h)
  world)

col
