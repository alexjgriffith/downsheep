(fn check-key-down [keys key]
  (if (-> keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn sign0 [x]
    (if (= x 0) 0
      (> x 0) 1
      (< x 0) -1))

(fn movement [dt world player keys]
  (let [lr (- (check-key-down keys :right) (check-key-down keys :left))
        ud (- (check-key-down keys :down) (check-key-down keys :up))
        speed player.speed
        x player.x
        y player.y
        max (if (and (~= 0 lr) (~= 0 ud)) speed.max-s2 speed.max)]
    (tset player :lr lr)
    (tset player :ud ud)
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed.decay speed.rate) lr)
                     (* dt (sign0 speed.x) (- speed.decay)))
                  (- max) max))
    (set speed.y (lume.clamp
                           (+ speed.y
                              (* dt (+ speed.decay speed.rate) ud)
                              (* dt (sign0 speed.y) (- speed.decay)))
                           (- max) max))
    (when (and (< (math.abs speed.x) speed.floor) (= 0 lr) )
      (set speed.x 0))
    (when (and (< (math.abs speed.y) speed.floor) (= 0 ud))
      (set speed.y 0))
    ;; (local col (require :prefab-col))
    (local (ax ay cols len) (world:move player
                                        (+ (+ x speed.x) 6)
                                        (+ (+ y speed.y) 12)
                                        player.collide))
    (when (or (~= ax (+ (+ x speed.x) 6)) (~= ay (+ (+ y speed.y) 12)))
      (tset player.speed :x 0)
      (tset player.speed :y 0))
    (tset player :x (- ax 6))
    (tset player :y (- ay 12))
    (values ax ay cols len)))

movement
