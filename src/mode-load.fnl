(local menu {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(var ui nil)

(local elements  [ {:type :button :y 25 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 1"}
                  {:type :button  :y 125 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 2"}
                  {:type :button  :y 225 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 3"}
                  {:type :button  :y 325 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 4"}
                  {:type :button  :y 425 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 5"}
                  {:type :button  :y 525 :oy -10 :ox 400 :w 350 :h 70 :text "Tutorial 6"}

                  {:type :button :y 25 :oy -10 :ox 0 :w 350 :h 70 :text "Level 1"}
                  {:type :button :y 125 :oy -10 :ox 0 :w 350 :h 70 :text "Level 2"}
                  {:type :button :y 235 :oy -10 :ox 0 :w 350 :h 70 :text "Level 3"}
                  {:type :button :y 325 :oy -10 :ox 0 :w 350 :h 70 :text "Level 4"}
                  {:type :button :y 425 :oy -10 :ox 0 :w 350 :h 70 :text "Level 5"}
                  {:type :button :y 525 :oy -10 :ox 0 :w 350 :h 70 :text "Level 6"}

                  {:type :button :y 25 :oy -10 :ox -400 :w 350 :h 70 :text "Level 7"}
                  {:type :button :y 125 :oy -10 :ox -400 :w 350 :h 70 :text "Level 8"}
                  {:type :button :y 235 :oy -10 :ox -400 :w 350 :h 70 :text "Level 9"}
                  {:type :button :y 325 :oy -10 :ox -400 :w 350 :h 70 :text "Level 10"}
                  {:type :button :y 425 :oy -10 :ox -400 :w 350 :h 70 :text "Level 11"}
                  ;; {:type :button :y 425 :oy -10 :ox -400 :w 350 :h 70 :text "Level 12"}

                  {:type :button :y 525 :oy -10 :ox -400 :w 350 :h 70 :text "User Level"}
                  {:type :button :y 625 :oy -10 :ox 0 :w 400 :h 70 :text "Back"}])


(local element-font
       {:title  ((. assets.fonts "avara") 70)
        :button  ((. assets.fonts "avara") 40)})

(local state (require :state))

(fn load-level [level]
  (assets.sounds.page:play)
  (tset state :current-level level)
  (tset state :menu false)
  (gamestate.switch (require "mode-game")))

(local element-click
       {"Tutorial 1"
        (fn [] (load-level "data-tutorial-1"))

        "Tutorial 2"
        (fn [] (load-level "data-tutorial-2"))

        "Tutorial 3"
        (fn [] (load-level "data-tutorial-3"))

        "Tutorial 4"
        (fn [] (load-level "data-tutorial-4"))

        "Tutorial 5"
        (fn [] (load-level "data-tutorial-5"))

        "Tutorial 6"
        (fn [] (load-level "data-tutorial-6"))

       "Level 1"
        (fn [] (load-level "data-level-1"))

        "Level 2"
        (fn [] (load-level "data-level-2"))

        "Level 3"
        (fn [] (load-level "data-level-3"))

        "Level 4"
        (fn [] (load-level "data-level-4"))

        "Level 5"
        (fn [] (load-level "data-level-5"))

        "Level 6"
        (fn [] (load-level "data-level-6"))

        "Level 7"
        (fn [] (load-level "data-level-7"))

        "Level 8"
        (fn [] (load-level "data-level-8"))

        "Level 9"
        (fn [] (load-level "data-level-9"))

        "Level 10"
        (fn [] (load-level "data-level-10"))

        "Level 11"
        (fn [] (load-level "data-level-11"))

        "Level 12"
        (fn [] (load-level "data-level-12"))

        "User Level"
        (fn [] (load-level "data-user-level"))

        "Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu" :load)))})

(local element-hover {:button (fn [element x y] :nothing)})

(set ui (buttons elements params element-click element-hover element-font))

(local mode-game (require :mode-game))

(fn menu.draw [self]
  (mode-game.draw self)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1 0.3)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw draw-canvas offsets.x offsets.y))

(fn menu.update [self dt]
  (mode-game.update self dt)
  (ui:update dt))

(fn menu.keypressed [self key code]
  (match key
    :f10 (when (not _G.web) (toggle-fullscreen))
    "escape"  (gamestate.switch (require :mode-menu))
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

menu
