(local menu {})

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))
(local state (require :state))

(local mode-game (require :mode-game))

(var has-entered-new-game false)

(var ui nil)

(local element-font
       {:title  ((. assets.fonts "avara") 100)
        :subtitle  ((. assets.fonts "avara") 40)
        :text  ((. assets.fonts "avara") 20)
        :button  ((. assets.fonts "avara") 40)})

(local element-click
       {"Quit Game"
        (fn []
          (love.event.quit))

        "New Game"
        (fn []
          (set has-entered-new-game true)
          (tset state :menu false)
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-game" :reset)))

        "New Level"
        (fn []
          (tset state :menu false)
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-game") :new-level))

        "Continue"
        (fn []
          (tset state :menu false)
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-game") :resume))

        "Restart Level"
        (fn []
          (tset state :menu false)
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") :reset))

        "Load Level"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-load")))

        "Editor"
        (fn []
          (assets.sounds.page:play)
          ;; (set-editor)
          (gamestate.switch (require "mode-editor")))

        "Controls"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-controls")))

        "Credits"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-credits")))

        "Options"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-options"))
          )})

(local element-hover {:button (fn [element x y] :nothing)})

(fn menu.init [self]
    (when (not state.loaded)
      (tset state :loaded true)
      (mode-game.enter self)))

(fn menu.enter [self _ from]
  (love.mouse.setVisible true)
  (tset state :menu true)
  (local opening [{:type :title :y 70 :w 400 :h 60 :text "Down Sheep"}
                  ;;{:type :title :y 170 :w 400 :h 60 :text "APOCALYPSE MEOW"}
                  {:type :subtitle :y 200 :w 400 :h 60 :text "( 2020 Fall Lisp Game Jam Submission )"}
                  {:type :button :y 300 :oy -10 :ox 400 :w 400 :h 70 :text "New Game"}
                  {:type :button :y 400 :oy -10 :ox 400 :w 400 :h 70 :text "Load Level"}
                  {:type :button :y 500 :oy -10 :ox 400 :w 400 :h 70 :text "Controls"}
                  {:type :button :y 600 :oy -10 :ox 400 :w 400 :h 70 :text "Quit Game"}
                  ])
  (local standard [{:type :title :y 30 :w 400 :h 60 :text "Down Sheep"}
                   {:type :button :y 200 :oy -10 :ox 400 :w 400 :h 70 :text "Continue"}
                   {:type :button :y 300 :oy -10 :ox 400 :w 400 :h 70 :text "Restart Level"}
                   {:type :button :y 400 :oy -10 :ox 400 :w 400 :h 70 :text "Load Level"}
                   {:type :button :y 500 :oy -10 :ox 400 :w 400 :h 70 :text "Controls"}
                   {:type :button :y 600 :oy -10 :ox 400 :w 400 :h 70 :text "Quit Game"}])
  (local elements
         (if (and has-entered-new-game (~= from :end-game))
             standard
             opening))
  (set ui (buttons elements params element-click element-hover element-font)))



(fn menu.draw [self]
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1 1)
  (mode-game.draw self)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.setColor 1 1 1 0.3)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setFont element-font.text)
  (love.graphics.printf "Art and Game By Alexander Griffith" 880 640 390 :right)
  (love.graphics.printf "Music By Thunder Hoodie" 880 680 390 :right)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  ;; (love.graphics.clear)
  (love.graphics.draw draw-canvas offsets.x offsets.y)
  )

(fn menu.leave [self]
  (love.graphics.setCanvas))

(fn menu.update [self dt]
  (mode-game.update self dt)
  (ui:update dt))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))
    :f10 (when (not _G.web) (toggle-fullscreen))
    :escape (if has-entered-new-game
                ((. element-click "Continue"))
                ((. element-click "New Game")))
    :. (gamestate.switch (require :mode-end-state))
    ))

menu
