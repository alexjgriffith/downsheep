{
  :data {
    :ground {
      :data {
        86 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        87 [0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 0]
        88 [0 2 3 3 3 3 0 0 0 0 2 4 4 4 2 0]
        89 [0 2 3 3 3 3 1 1 1 1 1 4 4 4 2 0]
        90 [0 2 3 3 3 3 1 1 1 1 1 4 4 4 2 0]
        91 [0 2 3 3 3 3 1 1 1 1 1 4 4 4 2 0]
        92 [0 2 3 3 3 3 0 0 0 0 2 4 4 4 2 0]
        93 [0 0 0 0 0 0 0 0 0 0 2 2 2 2 2 0]
        94 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "ground"
        2 "fence"
        3 "start"
        4 "finish"
      }
      :off-y 36
      :range {
        :maxx 94
        :maxy 52
        :minx 86
        :miny 37
      }
      :type "dense"
    }
    :objects {}
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 706.96074529375
        :y 318.78604031092
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 723.85357503851
        :y 316.92112217497
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}