(local controls {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(local controls-text
       "
'WSAD' - Move
'SPACE' + 'FEATHER' - Jump
'SPACE' + 'AXE' - Cut
'n' - Switch Sheep
'm' - Toggle Mute
'q' - Screenshot (Desktop Only)
'Esc' - Return to the menu
'Enter' - Enter editor mode (Desktop Only)
'F10' - Toggle Fullscreen (Desktop Only)")


(local elements
       [{:type :title :y 10 :w 400 :h 60 :text "CONTROLS"}
        {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Back" }
        {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text controls-text}])

(local element-font
       {:title  ((. assets.fonts "avara") 100)
        :subtitle  ((. assets.fonts "avara") 40)
        :button  ((. assets.fonts "avara") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local element-click
       {"Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :controls))})

(local element-hover {:button (fn [element x y] :nothing)})


(local ui (buttons elements params element-click element-hover element-font))

(local mode-game (require :mode-game))

(fn controls.draw [self]
  (mode-game.draw self)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1 0.3)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle "fill" 400 200 480 250)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw draw-canvas offsets.x offsets.y))

(fn controls.update [self dt]
  (mode-game.update self dt)
  (ui:update dt))

(fn controls.mousereleased [self ...]
  (ui:mousereleased ...))

(fn controls.keypressed [self key code]
  (match key
    :f10 (when (not _G.web) (toggle-fullscreen))
    "escape"  (gamestate.switch (require :mode-menu))
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))
    ))

controls
