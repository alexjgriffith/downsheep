(local quips {})

(tset quips :levels
      {"Tutorial 1"
        [{:who :sheep-1 :what "OH MY GOD GREGORY" :lines 1}
         {:who :sheep-2 :what "You ar'ght?" :lines 1 :short true}
         {:who :sheep-1 :what "WE'VE BECOME SHEEP" :lines 1}
         {:who :sheep-2 :what "Aye rek'n you're right" :lines 1}
         {:who :sheep-1 :what "WE HAVE TO AMEND THIS!" :lines 1}
         {:who :sheep-1 :what "But first lets go nibble some of that bright green grass down there" :lines 3}
         {:who :sheep-2 :what "Aye, I could do with some bright green grass" :lines 2}
         {:who :sheep-2 :what "Press 'n' when you need me" :lines 1 :duration 120}
         ]
       "Tutorial 2"
        [{:who :sheep-1 :what "WHAT IN TARNATION" :lines 1}
         {:who :sheep-2 :what "You ar'ght?" :lines 1 :short true}
         {:who :sheep-1 :what "WE'VE BEEN BAMBOOZLED" :lines 1}
         {:who :sheep-2 :what "Aye rek'n you're right *cough*" :lines 1}
         {:who :sheep-1 :what "HOW DID YOU GET IN THAT CAGE!" :lines 2}
         {:who :sheep-2 :what "*shrugs*" :lines 1 :short true}
         {:who :sheep-1 :what "Hold tight I'll try to get you out." :lines 1}
         {:who :sheep-1 :what "Then we can eat more grass!" :lines 1}
         ]
       "Tutorial 3"
        [{:who :sheep-1 :what "GREGORY! NOW I'M TRAPPED!" :lines 1}
         {:who :sheep-2 :what "You ar'ght?" :lines 1 :short true}
         {:who :sheep-1 :what "I'M SO HUNGRY" :lines 1}
         {:who :sheep-1 :what "Is that a dumbbell on the ground?" :lines 2 }
         ]
       "Tutorial 4"
        [{:who :sheep-1 :what "GREGORY! WHO PUT YOU ON THAT ISLAND!" :lines 2}
         {:who :sheep-2 :what "*shrugs*" :lines 1 :short true}
         {:who :sheep-1 :what "Is that a feather?" :lines 1}
         {:who :sheep-2 :what "Why don't you get it and try pressing space?" :lines 2 :duration 120}
         ]
       "Tutorial 5"
        [{:who :sheep-1 :what "SHEEP DON'T EAT TREES!" :lines 1}
         {:who :sheep-2 :what "Baaaaah" :lines 1 :short true}
         {:who :sheep-2 :what "ahem, I mean no" :lines 1}
         {:who :sheep-1 :what "Is that an axe?" :lines 1}
         {:who :sheep-2 :what "Why don't I get it and you try pressing 'space'." :lines 2 :duration 120}
         ]
       "Tutorial 6"
        [{:who :sheep-1 :what "WHY ARE WE HERE? AND WHY ARE WE SHEEP!?" :lines 2}
         {:who :sheep-2 :what "*Cough* *Cough*" :lines 1 :short true}
         {:who :sheep-1 :what "Gregory, you don't sound good. Being a sheep clearly does not agree with you!" :lines 3}
         {:who :sheep-1 :what "Maybe these flowers are the cure?" :lines 1 :duration 20}
         {:who :sheep-2 :what "Knock yourself out..." :lines 1}
         {:who :sheep-1 :what "Oh to have only two legs again!" :lines 1}
         ]
       "Level 1"
        [{:who :sheep-1 :what "IT HAPPENED AGAIN!" :lines 1}
         {:who :sheep-2 :what "What happened?" :lines 1 :short true}
         {:who :sheep-1 :what "We disappeared!" :lines 1}
         {:who :sheep-2 :what "I seem to be right here?" :lines 1}
         {:who :sheep-1 :what "Lets get more green grass!" :lines 1}
         {:who :sheep-1 :what "And collect all the flowers along the way!" :lines 2}
         ]
       "Level 2"
        [{:who :sheep-1 :what "GREGORY! SOMEONE IS PLAYING WITH US!" :lines 2}
         {:who :sheep-2 :what "Playing with us? *cough*" :lines 1 :short true}
         {:who :sheep-1 :what "Every time we begin to eat we disappear!" :lines 2}
         ]
       "Level 3"
        [{:who :sheep-1 :what "STOP THAT!" :lines 1}
         {:who :sheep-2 :what "Stop what?" :lines 1 :short true}
         {:who :sheep-1 :what "I wasn't talking to you Gregory." :lines 1}
         {:who :sheep-2 :what "Baaaaaah" :lines 1 :short true}
         {:who :sheep-2 :what "*cough* *cough*" :lines 1}
         {:who :sheep-1 :what "You're getting worse!" :lines 1}
         {:who :sheep-1 :what "We need those flowers!" :lines 1}
         ]
       "Level 4"
       [{:who :sheep-1 :what "GREGORY! HOW DID YOU END UP OVER THERE?" :lines 2}
        {:who :sheep-2 :what "*shrugs*" :lines 1 :short true}
        {:who :sheep-2 :what "*cough* *cough*" :lines 1}
        ]
       "Level 5"
       [{:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "Gregory, You don't sound so good!" :lines 1}
        {:who :sheep-1 :what "Lets get those flower's for you ASAP!" :lines 2 :short true}
        {:who :sheep-2 :what "Still on about those flowers?" :lines 1 :short true}
        {:who :sheep-1 :what "They are the only thing arround. They must be for something!" :lines 2}
        ]
       "Level 6"
       [{:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "Oh God, not again!" :lines 1}]
       "Level 7"
       [{:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "Hold on Gregory!" :lines 1}]
       "Level 8"
       [{:who :sheep-1 :what "I've got this feeling someone is watching us!" :lines 2}]
       "Level 9"
       [{:who :sheep-1 :what "I'm so hungry. And the green grass is so close!" :lines 2}
        {:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "We will get you flowers along the way!" :lines 2}]
       "Level 10"
       [{:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "Hold on Gregory!\nOnly a couple more levels!" :lines 2}
        {:who :sheep-2 :what "Baaahaaah, levels?" :lines 1}
        {:who :sheep-1 :what "*shrugs*" :lines 1 :short true}]
       "Level 11"
       [{:who :sheep-2 :what "*cough* *cough*" :lines 1 :short true}
        {:who :sheep-1 :what "Almost there!" :lines 1}
        {:who :sheep-1 :what "I just hope we've collected enough flowers!" :lines 2}]
       })

(tset quips :events
      {:finish
       {:sheep-1
        [{:who :sheep-1 :what "OH HOW LOVELY" :lines 1 :duration 2}
         {:who :sheep-1 :what "I COULD EAT THIS ALL DAY" :lines 1 :duration 2}
         {:who :sheep-1 :what "Food Finally!" :lines 1 :duration 2}
         {:who :sheep-1 :what "NUMNUMNUMNUMNUM" :lines 1 :duration 2}]
        :sheep-2
        [{:who :sheep-2 :what "Aye, It's good" :lines 1 :duration 2}
         {:who :sheep-2 :what "Can't complain." :lines 1 :duration 2}
         {:who :sheep-2 :what "My mum always told me to eat my greens" :lines 2 :duration 2}]}
       :pad-down
       {:sheep-1
        [{:who :sheep-1 :what "Clllick" :lines 1 :short true}
         {:who :sheep-1 :what "Something opened!" :lines 1 :short true}
         {:who :sheep-1 :what "What's that?" :lines 1 :short true}]
        :sheep-2
        [{:who :sheep-2 :what "Down it goes!" :lines 1 :short true}
         {:who :sheep-2 :what "Where's the gate?" :lines 1 :short true}]
        }

       :not-heavy
       {:sheep-1
        [{:who :sheep-1 :what "I'm too light!" :lines 1 :short true}]
        :sheep-2
        [{:who :sheep-2 :what "Need more weight!" :lines 1 :short true}]
        }

       :feather
       {:sheep-1
        [{:who :sheep-1 :what "Light as a feather" :lines 1 :duration 2}]
        :sheep-2
        [{:who :sheep-2 :what "I'm Up for a leap" :lines 1 :short true :duration 2}]
        }

       :weight
       {:sheep-1
        [{:who :sheep-1 :what "Sooo heavy" :lines 1 :short true :duration 3}
         {:who :sheep-1 :what "Heavy as a mountain" :lines 1 :short false :duration 3}
         {:who :sheep-1 :what "Ooof, so heavy!" :lines 1 :short false :duration 3}]
        :sheep-2
        [{:who :sheep-2 :what "That dumbbell is heavy" :lines 1 :short false :duration 3}]
        }

       :axe
       {:sheep-1
        [{:who :sheep-1 :what "WOW! That's sharp." :lines 1 :short false :duration 2}
         {:who :sheep-1 :what "Watch out trees!" :lines 1 :short false :duration 2}]
        :sheep-2
        [{:who :sheep-2 :what "Hmm, This is sharp." :lines 1 :short false :duration 2}]
        }

       :coin-pickup
       {:sheep-1
        [{:who :sheep-1 :what "I'VE GOT IT" :lines 1 :short true}
         {:who :sheep-1 :what "YOU'RE MINE" :lines 1 :short true}]
        :sheep-2
        [{:who :sheep-2 :what "In't she lovely" :lines 1 :short true}
         {:who :sheep-2 :what "Feeling a little better." :lines 1 :short true}]
        }

       :chop
       {:sheep-1
        [{:who :sheep-1 :what "WOW! That's sharp." :lines 1 :short false}
         {:who :sheep-1 :what "I HAVE BECOME DEATH" :lines 1 :short false}]
        :sheep-2
        [{:who :sheep-2 :what "A'right that was easy" :lines 1 :short false}
         {:who :sheep-2 :what "Timber" :lines 1 :short false}]
        }

       :leave
       {:sheep-1
        [{:who :sheep-1 :what "What the?" :lines 1 :short false}
         {:who :sheep-1 :what "But I'm soo hungry" :lines 1 :short false}
         {:who :sheep-1 :what "Again?" :lines 1 :short false}
         {:who :sheep-1 :what "Who is doing this?" :lines 1 :short false}
         {:who :sheep-1 :what "But I got to the food?" :lines 1 :short false}
         {:who :sheep-1 :what "Baaaah?" :lines 1 :short false}]
        :sheep-2
        [
         {:who :sheep-2 :what "..." :lines 1 :short false}]
        }
       }
      )

(tset quips :queue [])

(tset quips :index 1)

(tset quips :current-quip nil)

(tset quips :active-quip nil)

(tset quips :timer 0)

(tset quips :active-timer 0)

(tset quips :active true)

(tset quips :pos {:sheep-1 {:x 0 :y 0} :sheep-2 {:x 0 :y 0}})

(fn quips.next []
  (tset quips :index (+ quips.index 1))
  (tset quips :current-quip (. quips.queue quips.index)))

(fn quips.update [sheep-1 sheep-2 camera dt overwrite-story?]
  (tset quips :pos {:sheep-1 {:x (* (+ sheep-1.x camera.x) camera.scale)
                              :y (* (+ sheep-1.y camera.y) camera.scale)}
                    :sheep-2 {:x (* (+ sheep-2.x camera.x) camera.scale)
                              :y (* (+ sheep-2.y camera.y) camera.scale)}})

  (when (and (or quips.delay (> quips.total-timer 2)) (= quips.index 1))
    (tset quips :current-quip (. quips.queue quips.index)))

  (when quips.current-quip
    (tset quips :timer (+ quips.timer dt))
    (when (> quips.timer (or quips.current-quip.duration
                             (if  quips.current-quip.short 1
                                  (* 2 quips.current-quip.lines))))
      (tset quips :timer 0)
      (quips.next)))
  (when overwrite-story?
    (tset quips :current-quip nil)
    (tset quips :index 1000))
  (when quips.active-quip
    (tset quips :active-timer (+ quips.active-timer dt))
    (when  (> quips.active-timer (or quips.active-quip.duration 0.4))
      (tset quips :active-timer 0)
      (tset quips :active-quip nil)))
  (tset quips :total-timer (+ quips.total-timer dt)))


(fn quips.load-level [level]
  (tset quips :queue [])
  (tset quips :timer 0)
  (tset quips :total-timer 0)
  (tset quips :index 1)
  (tset quips :current-quip nil)
  (tset quips :active-quip nil)
  (when (. quips.levels level)
    (tset quips :delay (if (= level "Tutorial 1") 0.5 2))
    (each [_ quip (pairs (. quips.levels level))]
      (tset quip :time 0)
      (table.insert quips.queue quip))))

(fn quips.event [event sheep]
  (tset quips :active-timer 0)
  (local quip (lume.clone (lume.randomchoice (. quips.events event sheep))))
  (tset quips :active-quip quip))

(local ui-font (_G.assets.fonts.avara 30))
(fn quips.draw []
  (when quips.current-quip
    (local current-sheep quips.current-quip.who)
    (local w (if (and quips.current-quip.what (= 1 quips.current-quip.lines))
                 (math.max
                  40
                  (math.floor (+ 40 (ui-font:getWidth quips.current-quip.what))))
                 500))
    (local h (+ 3 (* 40 quips.current-quip.lines)))
    (local px (+ (. quips.pos current-sheep :x) 32))
    (local py (. quips.pos current-sheep :y))
    (local x (- px (/ w 2)))
    (local y (- py h 5))
    (local a1 px)
    (local b1 py)
    (local a2 (- px 20))
    (local b2 (- py 5))
    (local a3 (+ px 20))
    (local b3 (- py 5))
    (love.graphics.setColor 1 1 1 0.5)
    (love.graphics.rectangle "fill" x y w h)
    (love.graphics.polygon "fill" a1 b1 a2 b2 a3 b3)
    (love.graphics.setColor 0 0 0 1)
    (love.graphics.setFont ui-font)
    (love.graphics.printf quips.current-quip.what (+ x 2) (+ y 5) (- w 4) :center)
    (love.graphics.setColor 1 1 1 1))
  (when (and quips.active-quip (not quips.current-quip))
    (local current-sheep quips.active-quip.who)
    (local w (if (or (not quips.active-quip.lines)
                     (= quips.active-quip.lines 1))
                 (math.max
                  40
                  (math.floor (+ 40
                                 (ui-font:getWidth quips.active-quip.what))))
                 540))
    (local h (+ 3 (* 40 quips.active-quip.lines)))
    (local px (+ (. quips.pos current-sheep :x) 32))
    (local py (. quips.pos current-sheep :y))
    (local x (- px (/ w 2)))
    (local y (- py h 5))
    (local a1 px)
    (local b1 py)
    (local a2 (- px 20))
    (local b2 (- py 5))
    (local a3 (+ px 20))
    (local b3 (- py 5))
    (love.graphics.setColor 1 1 1 0.5)
    (love.graphics.rectangle "fill" x y w h)
    (love.graphics.polygon "fill" a1 b1 a2 b2 a3 b3)
    (love.graphics.setColor 0 0 0 1)
    (love.graphics.setFont ui-font)
    (love.graphics.printf quips.active-quip.what (+ x 2) (+ y 5) (- w 4) :center)
    (love.graphics.setColor 1 1 1 1)))

quips
