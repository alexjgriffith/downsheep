(local map (require :lib.map))
(local subtile (require :lib.subtile))


(local editor {})

(fn make-brush [tile tile-type layer image-w image-h sprite]
  (fn [mapin x y delete?]
    (local details (. states tile))
    (local map-tile (map.tile tile tile-type w h image-w image-h sprite))
    (if (not delete?)
        (map.add-tile mapin x y layer
                      map-tile
                      (when details (lume.clone details)))
        (map.remove mapin x y layer))))

;; put all heavy functions in here
;; it will not be run in game mode
(fn editor.update [self level states]
  (tset self :brushes (tile-data-to-brushes self.tile-data states))
  (local {: tileset-batch : tiles : level : grid-size
          : tile-size : tile-display-width : tile-display-height
          : tile-layers : object-layers : states} self)
  (map.update-tileset-batch tileset-batch tiles level tile-size tile-layers)
  )

(fn editor.load-level [self level-file?]
  (let [{: tileset-batch  : tiles : level : grid-size
         : tile-size : tile-display-width : tile-display-height
         : tile-layers  : states} self]
    (if level-file?
        (let [level (if (and (love.filesystem.isFused)
                             (love.filesystem.exists (.. level-file? ".fnl")))
                        (fennel.eval (love.filesystem.read (.. level-file? ".fnl")) {})
                        (require level-file?))]
          (map.update-tileset-batch
           tileset-batch tiles level
           tile-size  tile-layers)
          (update-objects level.data.objs states)
          level)
        (map.new tile-display-width tile-display-height tile-layers tile-size))))


(fn editor.add-tile [self brush x y not-interactive?]
  (let [{: level
         : tileset-batch
         : tiles
         : tile-size
         : tile-layers
         : brushes} self]
    (local tile ((. brushes brush) level x y))
    (when (not not-interactive?)
      (map.update-neighbours level :ground)
      ;; (map.update-tileset-batch object-batch tiles level tile-size object-layers)
      ;; you don't have to wory about updating object, this will be
      ;; handled in game code
      (map.update-tileset-batch tileset-batch tiles level tile-size tile-layers)

      ;; remove all other references to player in level
      (love.event.push :edit-map  :add-tile tile tile-size level))
    ))

(fn editor.remove-tile [self brush x y]
  (let [{: level
         : tileset-batch
         : tiles
         : tile-size
         : tile-layers
         : brushes} self]
    (local tile ((. brushes brush) level x y :delete))
    (map.update-neighbours level :ground)
    ;; handled in the draw call!
    ;; (map.update-sprites sprites sprite-quads level sprite-size [:obj])
    ;; (map.update-tileset-batch object-batch tiles level tile-size object-layers)
    (map.update-tileset-batch tileset-batch tiles level tile-size tile-layers)
    (love.event.push :edit-map  :remove-tile tile tile-size level)
    ))

(fn editor.hover [self brush x y]
  (let [{: level
         : hover-batch
         : tiles
         : tile-size
         : all-layers
         : brushes} self]
    (local hovermap (map.new level.width level.height all-layers tile-size))
    (local tile ((. brushes brush) hovermap x y))
    (map.update-neighbours hovermap :ground)
    ;; (db hovermap)
    ;; (db hovermap)
    (map.update-tileset-batch hover-batch tiles hovermap tile-size [:ground :objs])
    ))

(fn editor.save [self file]
  ;; try to remove -infs
  (local minf (/ 1 0))
  (local m-inf (/ -1 0))
  (each [key hash (pairs self.level.data)]
    (local new-hash {})
    (each [id tile (pairs hash)]
      (when (and (~= m-inf id ) (~= minf id ))
        (tset new-hash id tile)))
    (tset self.level.data key new-hash)
    )
  (let [f (assert (io.open file "wb"))
        c (: f :write ((require "lib.fennelview") self.level))]
    (: f :close)
    c))

(local editor-mt {
                  :__index editor
                  :update editor.update
                  :load-level editor.load-level
                  :add-tile editor.add-tile
                  :remove-tile editor.remove-tile
                  :hover editor.hover
                  :save editor.save})

;; map-width map-height tile-size grid-size tile-layers object-layers
;; all-layers scale tile-display-width tile-display-hegiht
;; tile-sheet tile-data

(fn [level-file tile-sheet sprite-key tile-data states options]
  (fn pixel-to-tile [px tile-size]
    (math.ceil (/ px tile-size)))
  (local default
         {:map-width 960
          :map-height 540
          :tile-size 16 ; two grids per tile. Confusing, I KNOW!
          :grid-size 8
          :tile-layers [:ground]
          :object-layers [:objs]
          :all-layers [:ground :for1 :objs]
          ;; :scale 1 ; trying to root this bugger out
          })
  (each [key value (pairs (or options {}))]
    (tset default key value))
  ;; should load multiple image files but need to find a way
  ;; to deal with hover (perhaps batch image is NOT the way
  ;; to go for it)
  ;;(local tile-sheet (love.graphics.newImage tile-image-file))
  ;;(local object-sheet (love.graphics.newImage tile-image-file))
  ;;(local tile-sheet (love.graphics.newImage "assets/tiles/tiles-and-objects (Object).png"))
  (local tileset-batch (love.graphics.newSpriteBatch
                        (. tile-sheet sprite-key) 400))
  ;; (local object-batch (love.graphics.newSpriteBatch
  ;;                      (. tile-sheet sprite-key) 100))
  (local hover-batch (love.graphics.newSpriteBatch
                      (. tile-sheet sprite-key) 20))
  (local sample-grid
         (subtile.newGrid
          default.grid-size default.grid-size
          (: (. tile-sheet sprite-key) :getWidth)
          (: (. tile-sheet sprite-key) :getHeight)))

  ;; realy should rename tiles quads!
  (local tiles (tile-data-to-tiles tile-data sample-grid))
  (local brushes (tile-data-to-brushes tile-data states))
  (local level
         (setmetatable
          {:tile-sheet tile-sheet
           :sample-grid sample-grid
           :tile-data tile-data
           :states (or states {})
           :sprite-key sprite-key
           :tile-size default.tile-size
           :tile-display-width
           (pixel-to-tile default.map-width  default.tile-size)
           :tile-display-height
           (pixel-to-tile default.map-height  default.tile-size)
           :map-width default.map-width
           :map-height default.map-height
           :tileset-batch tileset-batch
           :hover-batch hover-batch
           :level nil
           :active true
           :grid-size default.grid-size
           :object-layers default.object-layers
           :tile-layers default.tile-layers
           :all-layers default.all-layers
           :tiles tiles
           :render true
           :brushes brushes
           :brush-keys (lume.keys brushes)
           } editor-mt))
  (tset level :level (level:load-level level-file))
  level)
