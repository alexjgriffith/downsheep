(local decal {})

(fn decal.collide [decal]
  (tset decal :active false)
  decal)

(fn decal.update [decal dt])

(fn decal.draw [decal camera editor?]
  (when (and decal decal.active camera)
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    ;; (pp editor?)
    (local quad (. decal (if editor? :editor-quad :quad)))
    (love.graphics.draw decal.image quad
                        (math.floor (* (+ camera.x decal.x) camera.scale))
                        (math.floor (* (+ camera.y decal.y) camera.scale))
                        0
                        camera.scale
                        camera.scale)))

(local decal-mt {:__index decal
                :update decal.update
                :collide decal.collide
                :draw decal.draw})

;; obj {:type decal :colour :yellow :x 10 :y 10}
(fn decal.new [item x y]
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos ?w ?h]
    (love.graphics.newQuad pos.x pos.y (or ?w 24) (or ?h 24) 256 256))
  (local quad
         (match item
           :axe (newQuad {:x 0 :y 64})
           :weight (newQuad {:x 0 :y 88})
           :feather (newQuad {:x 0 :y 112})))
  (local editor-quad
         (match item
           :axe (newQuad {:x 32 :y 96} 8 8)
           :weight (newQuad {:x 40 :y 96} 8 8)
           :feather (newQuad {:x 48 :y 96} 8 8)))
  (setmetatable {: x : y : item : quad : image
                 : editor-quad
                 :w 24 :h 24
                 :editor-w 8
                 :editor-h 8
                 :type :decal
                 :name (.. :decal " " item)
                 :active true} decal-mt))

decal
