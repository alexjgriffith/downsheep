{
  :data {
    :ground {
      :data {
        67 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        68 [0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0]
        69 [0 0 0 0 0 0 1 1 1 1 1 0 1 1 1 1 1 1 0 0 0 0 0 0 0 0]
        70 [0 0 0 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 0 0 0 0 0 0 0 0]
        71 [0 0 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 0 0 0 0 0 0 0 0]
        72 [0 0 1 1 0 0 0 1 1 1 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0]
        73 [0 0 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
        74 [0 0 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0]
        75 [0 0 1 1 0 0 0 1 1 1 0 0 0 0 1 1 1 0 1 1 0 0 0 0 0 0]
        76 [0 0 1 1 0 0 0 1 1 1 0 0 0 0 1 1 1 0 1 1 0 0 0 0 0 0]
        77 [0 1 1 0 0 0 1 1 1 1 1 0 0 0 1 1 0 0 0 1 0 0 0 0 0 0]
        78 [0 1 1 0 0 0 1 1 1 1 1 0 0 0 1 1 0 0 0 1 0 1 1 1 1 0]
        79 [0 1 1 0 0 0 1 1 1 1 1 0 0 1 1 1 1 0 0 1 0 1 1 1 1 0]
        80 [0 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0]
        81 [0 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0]
        82 [0 0 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 1 0 1 1 1 1 0]
        83 [0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 0]
        84 [0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 0]
        85 [0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0]
        86 [0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0]
        87 [0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 4 0]
        88 [0 0 1 1 0 0 0 0 0 1 1 1 0 1 1 1 1 1 1 0 1 2 2 2 4 0]
        89 [0 0 1 1 1 1 1 1 1 1 1 1 1 1 3 3 3 3 1 0 1 2 2 2 4 0]
        90 [0 0 0 1 1 1 1 1 1 1 1 1 1 1 3 3 3 3 1 0 1 2 2 2 4 0]
        91 [0 0 0 0 0 0 0 0 0 1 1 1 0 1 3 3 3 3 1 0 1 2 2 2 4 0]
        92 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 3 3 3 3 0 0 0 1 1 1 4 0]
        93 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 3 3 3 3 0 0 0 0 0 0 0 0]
        94 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0]
        95 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 4 1 4 4 0 0 0 0 0 0 0]
        96 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 1 1 1 4 0 0 0 0 0 0 0]
        97 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 1 1 1 4 0 0 0 0 0 0 0]
        98 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 1 1 1 4 0 0 0 0 0 0 0]
        99 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 4 4 4 4 0 0 0 0 0 0 0]
        100 [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
      }
      :map {
        0 "chasm"
        1 "ground"
        2 "finish"
        3 "start"
        4 "fence"
      }
      :off-y 29
      :range {
        :maxx 100
        :maxy 55
        :minx 67
        :miny 30
      }
      :type "dense"
    }
    :objects {
      11 {
        :item "weight"
        :type "decal"
        :unique true
        :x 632
        :y 416
      }
      12 {
        :colour "pink"
        :type "pad"
        :unique true
        :x 640
        :y 352
      }
      15 {
        :colour "pink"
        :type "gate"
        :x 664
        :y 360
      }
      16 {
        :colour "pink"
        :type "gate"
        :x 648
        :y 328
      }
      17 {
        :colour "pink"
        :type "gate"
        :x 640
        :y 328
      }
      18 {
        :type "tree"
        :x 616
        :y 392
      }
      19 {
        :item "feather"
        :type "decal"
        :unique true
        :x 616
        :y 288
      }
      20 {
        :item "axe"
        :type "decal"
        :unique true
        :x 552
        :y 352
      }
      23 {
        :colour "blue"
        :type "gate"
        :x 616
        :y 360
      }
      24 {
        :colour "blue"
        :type "gate"
        :x 616
        :y 352
      }
      25 {
        :colour "blue"
        :heavy true
        :type "pad"
        :unique true
        :x 648
        :y 312
      }
      26 {
        :colour "red"
        :type "gate"
        :x 584
        :y 312
      }
      27 {
        :colour "red"
        :type "gate"
        :x 584
        :y 304
      }
      28 {
        :colour "red"
        :type "gate"
        :x 584
        :y 296
      }
      29 {
        :colour "red"
        :type "pad"
        :unique true
        :x 600
        :y 392
      }
      30 {
        :type "tree"
        :x 560
        :y 280
      }
      31 {
        :type "tree"
        :x 568
        :y 280
      }
      32 {
        :type "tree"
        :x 712
        :y 304
      }
      33 {
        :type "tree"
        :x 720
        :y 304
      }
      34 {
        :colour "yellow"
        :type "gate"
        :x 656
        :y 392
      }
      35 {
        :colour "pink"
        :type "gate"
        :x 648
        :y 376
      }
      36 {
        :type "tree"
        :x 680
        :y 376
      }
      37 {
        :colour "pink"
        :type "gate"
        :x 640
        :y 376
      }
      1 {
        :type "tree"
        :x 760
        :y 368
      }
      2 {
        :colour "yellow"
        :type "coin"
        :unique true
        :x 768
        :y 360
      }
      3 {
        :colour "pink"
        :type "coin"
        :unique true
        :x 776
        :y 368
      }
      4 {
        :colour "orange"
        :type "coin"
        :unique true
        :x 784
        :y 376
      }
      5 {
        :colour "blue"
        :type "coin"
        :unique true
        :x 784
        :y 368
      }
      6 {
        :colour "red"
        :type "coin"
        :unique true
        :x 768
        :y 376
      }
      7 {
        :colour "white"
        :type "coin"
        :unique true
        :x 784
        :y 360
      }
      8 {
        :colour "yellow"
        :type "pad"
        :unique true
        :x 728
        :y 328
      }
      9 {
        :colour "yellow"
        :type "gate"
        :x 664
        :y 368
      }
    }
    :players {
      "sheep one" {
        :colour "white"
        :name "sheep one"
        :x 737.77719126514
        :y 359.5754470891
      }
      "sheep two" {
        :colour "white"
        :name "sheep two"
        :x 713.80955916329
        :y 348.06532965798
      }
    }
  }
  :height 180
  :id 0
  :tile-set {
    :chasm {
      :auto "blob"
      :collidable "fall"
      :layer "ground"
      :pos [1 3]
      :size "square10"
    }
    :fence {
      :auto "fence"
      :collidable "stop"
      :layer "ground"
      :pos [1 1]
      :size "square16"
    }
    :finish {
      :auto "blob"
      :collidable "finish"
      :layer "ground"
      :pos [1 6]
      :size "square10"
    }
    :forest {
      :auto "blob"
      :collidable "stop"
      :layer "ground"
      :pos [1 14]
      :size "square10"
    }
    :ground {
      :auto "fixed"
      :collidable "none"
      :layer "ground"
      :pos [1 12]
      :size "square4"
    }
    :start {
      :auto "blob"
      :collidable "start"
      :layer "ground"
      :pos [1 9]
      :size "square10"
    }
  }
  :tile-size 8
  :width 180
}