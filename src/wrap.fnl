(global gamestate (require :lib.gamestate))
(global params (require :params))

(local repl (require :lib.stdio))
(local cargo (require :lib.cargo))

(tset _G :web true)

(math.randomseed (os.time))


(fn love.load [args uargs]
  (local (x y flags) (love.window.getMode))
  (love.window.setMode (* 4 x) (* 4 y) flags)
  (when (= :web (. args 1))
    (tset _G :web true))
  (love.graphics.setBackgroundColor params.colours.black)
  (love.filesystem.setIdentity "downsheep")
  (when (not (love.filesystem.getInfo (love.filesystem.getSaveDirectory)))
    (love.filesystem.createDirectory (love.filesystem.getSaveDirectory))
    (pp (.. (love.filesystem.getSaveDirectory) " created.")))
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (tset _G :assets (cargo.init {:dir :assets
                                :processors
                                {"sounds/"
                                 (fn [sound _filename]
                                   (sound:setVolume 0.1)
                                   sound)
                                 }}))
  (local bgm (if _G.web
                 (love.audio.newSource "assets/music/Thunder Hoodie Beats - Isolated.mp3" :static)
                 (. _G.assets.music "Thunder Hoodie Beats - Isolated")))
  (bgm:setLooping true)
  (bgm:setVolume 0.25)
  (bgm:play)
  (_G.assets.sounds.bounce:setVolume 0.5)
  (require :globals)
  (require :handlers)
  (gamestate.registerEvents)
  (gamestate.switch (require :mode-menu) :wrap)
  (when (not _G.web) (repl.start)))
