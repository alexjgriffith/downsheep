(local gate {})

(fn gate.collide [gate])

(fn gate.update [gate dt])

(fn gate.draw [gate camera]
  (when (and gate camera)
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    (local quad (if gate.active gate.up-quad gate.down-quad))
    (love.graphics.draw gate.image quad
                        (math.floor (* (+ camera.x gate.x) camera.scale))
                        (math.floor (* (+ camera.y gate.y) camera.scale))
                        0
                        camera.scale
                        camera.scale)))

(local gate-mt {:__index gate
                :update gate.update
                :collide gate.collide
                :draw gate.draw})

;; obj {:type gate :colour :yellow :x 10 :y 10}
(fn gate.new [colour number x y]
  ;; down = active
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos]
    (love.graphics.newQuad pos.x pos.y 8 8 256 256))
  (local quads
         (match colour
     :yellow (lume.map [{:x 96 :y 0} {:x 104 :y 0}] newQuad)
     :pink (lume.map [{:x 96 :y 8} {:x 104 :y 8}] newQuad)
     :orange (lume.map [{:x 96 :y 16} {:x 104 :y 16}] newQuad)
     :blue (lume.map [{:x 96 :y 24} {:x 104 :y 24}] newQuad)
     :red (lume.map [{:x 96 :y 32} {:x 104 :y 32}] newQuad)
     :white (lume.map [{:x 96 :y 40} {:x 104 :y 40}] newQuad)))
  (local down-quad (. quads 2))
  (local up-quad (. quads 1))
  (setmetatable {: x : y : colour : down-quad : up-quad : image
                 :w 8 :h 8
                 :type :gate
                 :name (.. :gate " " colour " " number)
                 :active true} gate-mt))

gate
