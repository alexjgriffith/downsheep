(local tree {})

(fn tree.collide [tree])

(fn tree.update [tree dt])

(fn tree.draw [tree camera]
  (when (and tree camera)
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    (local quad (if (= tree.type :tree) tree.up-quad tree.down-quad))
    (love.graphics.draw tree.image quad
                        (math.floor (* (+ camera.x tree.x) camera.scale))
                        (math.floor (* (+ camera.y tree.y) camera.scale))
                        0
                        camera.scale
                        camera.scale)))

(local tree-mt {:__index tree
                :update tree.update
                :collide tree.collide
                :draw tree.draw})

;; obj {:type tree :colour :yellow :x 10 :y 10}
(fn tree.new [number x y]
  ;; down = active
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos]
    (love.graphics.newQuad pos.x pos.y 8 8 256 256))
  (local down-quad (newQuad {:x 32 :y 24}))
  (local up-quad (newQuad {:x 32 :y 16}))
  (setmetatable {: x : y : colour : down-quad : up-quad : image
                 :w 8 :h 8
                 :type :tree
                 :name (.. :tree " " number)
                 :active true} tree-mt))

tree
