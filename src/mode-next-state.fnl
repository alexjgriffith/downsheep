(local next-state {})

(var timer 0)

(local state (require :state))

(var step :leave)

(var alpha 1)

(local gamestate (require :lib.gamestate))

(local params (require :params))

(local mode-game (require :mode-game))

(local levels (require :levels))

(fn next-state.draw [self]
  (mode-game.draw self)
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.clear)
  (when (~= 0 alpha)
    (love.graphics.setColor 1 1 1 (lume.clamp alpha 0 1))
    (love.graphics.rectangle "fill" 0 0 1280 780))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle "line" 1 1 1278 718)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw draw-canvas offsets.x offsets.y)
  (when (= step :done) (gamestate.switch (require :mode-game) :resume)))


(fn next-state.update [self dt]
  ;; (mode-game.update self dt)
  (mode-game.update self dt)
  (match step
    :leave
    (do
      (if (> timer 1)
          (do
            (set step :load)
            (set timer 0)
            (set alpha 1)
            (levels.next state))
          (do
            (set timer (+ timer dt))
            (set alpha timer)))
      )
    :load (if (> timer 1)
              (do
                (set step :enter)
                (set timer 0))
              (do
                (set timer (+ timer dt))))
    :enter
    (do
      (tset state :menu false)
      (if (< alpha 0)
          (do
            (set timer 0)
            (set step :done)
            (set alpha 0)
            )
          (do (set timer (+ timer dt))
              (set alpha (/ (- 2 timer) 2)))
      ))
    )
  )

(fn next-state.enter [self from ...]
  (love.mouse.setVisible false)
  (set step :leave)
  (tset state :menu true)
  (when (not (. levels :list state.current-level :next-level))
    (gamestate.switch (require :mode-end-state)))
  )

(fn next-state.leave [self from ...]
  (tset state :menu false))

(fn next-state.mousereleased [self ...])

(fn next-state.keypressed [self key code]
  (match key
    :f10 (when (not _G.web) (toggle-fullscreen))
    "m" (toggle-sound)
    "q" (when (not _G.web) (screenshot))
    "n" (when (= :enter step )
          (if state.sheep-1.active
             (do (tset state.sheep-1 :active false)
                 (tset state.sheep-2 :active true))
             (do (tset state.sheep-1 :active true)
                 (tset state.sheep-2 :active false))
             ))
    ))

next-state
