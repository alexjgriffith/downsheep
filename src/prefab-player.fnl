(local player {})

(local movement (require :movement))

(local default-collide
       {:camera "bounce"
        :fence "slide"
       :sheep "slide"
       :chasm "slide"
       :finish "cross"
       :start "cross"
       :forest "slide"
       :coin "cross"
       :pad "cross"
       :gate "slide"
       :open-gate "cross"
       :decal "cross"
       :tree "slide"
       :cut-tree "cross"})

(fn player.collide [player other]
  (. default-collide other.type))

(fn player.collide/1 [other]
  (. default-collide other.type))

(fn player.collide-dash [player other]
  (. player.collide-array other.type))

(fn check-dashcut [player world type]
  (fn query [x y what] (world:querySegment (+ player.x 8)
                                           (+ player.y 14)
                                           (+ player.x 8 x)
                                           (+ player.y 14 y)
                                           (fn [other] (= what other.type))))
  (fn check [x y] (world:queryRect (+ player.x 6 x) (+ player.y 12 y) 4 4
                                   (fn [other] (= :slide (player.collide/1 other)))))
  (let [(_ right) (query 4 0 type)
        (_ left) (query -4 0 type)
        (_ top) (query 0 -4 type)
        (_ bottom) (query 0 4 type)
        r (and (> right 0)  (= 0 (# (check 16 0))))
        l (and (> left 0)  (= 0 (# (check -16 0))))
        t (and (> top 0) (= 0 (# (check 0 -16))))
        d (and (> bottom 0) (= 0 (# (check 0 16))))]
    (if l (values true {:x -12 :y 0})
        r (values true {:x 12 :y 0})
        t (values true {:x 0 :y -12})
        d (values true {:x 0 :y 12})
        (values false {:x 0 :y 0})
        )
    )
  )

(fn post-collision [player other world]
  (match other.type
    :coin (love.event.push :coin-pickup other)
    :pad (love.event.push :pad-down other)
    :decal (love.event.push :step-on-decal other)
    :tree (love.event.push :chop-tree player.has-axe player.dashcut-down other)
    ;; :chasm (pp (check-dashcut player other world :chasm))
    ;; :chasm (love.event.push :leap player.chop player.dashcut-down other)
    :finish (love.event.push :on-finish player)))

(fn check-key-down [keys key]
  (if (-> keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn move [player world keys dt]
  (let [lr (- (check-key-down keys :right) (check-key-down keys :left))
        ud (- (check-key-down keys :down) (check-key-down keys :up))]
    (when (< lr 0)
      (tset player :flipped true))
    (when (> lr 0)
      (tset player :flipped false))
    (movement dt world player keys)))

(fn player.update [player world keys dt]
  ;; has to come before we reset on-finish
  (let [eating player.eating]
      (if player.on-finish
          (do (eating:setLinearAcceleration -500 -700 500 200)
              (eating:setEmissionRate 400)
              (tset player :draw-on-finish true))
          (do (eating:setEmissionRate 0)
              (eating:reset)))
      (eating:setParticleLifetime 0.4 0.5)
      (eating:update dt))


  (when player.active
    (tset player :on-finish false)
    (local dashcut-down (love.keyboard.isDown :space))
    (when (not dashcut-down)
      (tset player :dashcut-down false))
    (when (and dashcut-down (not player.dashcut-down))
      (tset player :dashcut-down true)
      (local (can-jump dir-jump) (check-dashcut player world :chasm))
      (local (can-cut dir-cut) (check-dashcut player world :tree))
      ;; (tset player :has-feather true)
      (if (and can-jump player.has-feather)
          (do
            (tset player.collide-array :chasm nil)
            (local (ax ay cols) (world:move player
                                            (+ 6 player.x dir-jump.x)
                                            (+ 12 player.y dir-jump.y)
                                            player.collide-dash))
            (tset player :x (- ax 6))
            (tset player :y (- ay 12))
            (love.event.push :jump player dir-jump))
          (and can-cut player.has-axe)
          (do
            (do
              ;;(tset player.collide-array :trees nil)
              ;; (love.event.push :chop-tree player.has-axe player.dashcut-down other)
              (local (ax ay cols) (world:move player
                                              (+ 6 player.x dir-cut.x)
                                              (+ 12 player.y dir-cut.y)
                                              player.collide-dash))
              (each [_ col (pairs cols)]
                (post-collision player col.other world))
              (tset player :x (- ax 6))
              (tset player :y (- ay 12)))))
      )
    (local (_ax _ay cols len) (move player world keys dt))
    (each [_ col (pairs cols)]
      (post-collision player col.other world))
    (when (< player.speed.x 0)
        (tset player :flipped true))
    (when (> player.speed.x 0)
      (tset player :flipped false))
    ;; (when (> len 0)
    ;;       )
    ;;   )
    )
  (local speed2 (+ (* player.speed.x player.speed.x) (* player.speed.y player.speed.y)))
  (local moving (> speed2 player.speed.floor))

  (local next-time (+ player.timer dt))
  (if (> next-time player.bpm)
      (tset player :timer (- next-time player.bpm))
      (tset player :timer next-time))
  (local mid (/ player.bpm 2))
  (local rotate  (/ (- player.timer mid) mid))
  (local squash (math.abs rotate))
  (tset player.squash :y (-  1  (* (if player.active 0.1 0) squash)))
  (tset player.squash :x (- 1 (* (if player.active 0.03 0)  squash)))
  (tset player :rotate 0)
  (if (and moving player.active)
      (do (tset player :bpm (/ 1 (/ 160 60)))
          (when (not player.moving)
            (tset player :moving true)
            (tset player :timer 0))
          (if player.flipped
              (tset player :rotate (* (/ math.pi 8) rotate))
              (tset player :rotate (- (* (/ math.pi 8) rotate)))))
      (do
        (tset player :moving false)
        (tset player :rotate 0)
        (tset player :bpm (/ 1 (/ 160 60 8)))))
  ;; particles
  (local psystem player.psystem)
  (if (and moving player.active)
      (do (psystem:setSizeVariation 0.8)
          (psystem:setEmissionRate 200)
          (psystem:setParticleLifetime 0.4 0.5)
          )
      (psystem:setEmissionRate 0))
  (let [x player.lr
        y player.ud
        floor player.speed.floor
        max (if (and (~= 0 lr) (~= 0 ud)) (/ 1 (math.sqrt 2)) 1)]     ;; 1/ sqrt 2 -> 1^2/2 -> 1/2
    ;; (psystem:setLinearAcceleration
     ;; (if (> x 0) (* max -1000) -200)
     ;; (if (> y 0) (* max -1000) -200)
     ;; (if (< x 0) (* max 1000) 200)
     ;; (if (< y 0) (* max 1000) 200)
    ;; )
    (local pi-times (match [x y]
                      [-1 0] 0
                      [ -1 -1] 0.25
                      [0 -1] 0.5
                      [1 -1] 0.75
                      [1 0] 1
                      [1 1] 1.25
                      [0 1] 1.5
                      [-1 1] 1.75
                      [_ _] 0))
    (psystem:setDirection  (* pi-times math.pi) )
    (psystem:setSpeed (if (or (~= y 0) (~= x 0)) (* max 600)  0)
                      (if (or (~= y 0) (~= x 0)) (* max 800)  0)
                      ))
  (psystem:setSpin 1)
  (psystem:setLinearAcceleration 0 -300 0 100)
  (psystem:setSpread (/ math.pi 8))
  (psystem:setLinearDamping 10 )
  (player.psystem:update dt)
  )

(fn player.draw [player camera]
  ;; (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (local quad (if player.active player.quad player.quad-down))
  (love.graphics.setColor 1 1 1 0.5)
  (when (and player.active (not player.dashcut-down))
    (love.graphics.draw player.image player.shadow-quad
                        (math.floor (* (+ camera.x player.x (if player.flipped 16 0)) camera.scale))
                        (math.floor (* (+ camera.y player.y 1 player.hop) camera.scale))
                        0
                        (* (if player.flipped -1 1) (* 1 camera.scale))
                        (* 1 camera.scale)))
  ;; Particles
  ;; (when (not player.draw-on-finish)
  ;;   (love.graphics.setColor 1 1 1 0))
  (love.graphics.draw player.eating
                      (math.floor (* (+ camera.x player.x 8 (if player.flipped 0 0)) camera.scale))
                      (math.floor (* (+ camera.y player.y 12 player.hop) camera.scale)))


  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw player.psystem
                      (math.floor (* (+ camera.x player.x 8 (if player.flipped 0 0)) camera.scale))
                      (math.floor (* (+ camera.y player.y 12 player.hop) camera.scale)))
  ;; sheep
  (love.graphics.draw
   player.image quad
   (math.floor (* (+ camera.x player.x 8 (if player.flipped 0 0)) camera.scale))
   (math.floor (* (+ camera.y player.y player.hop 16) camera.scale))
   player.rotate
   (* (if player.flipped -1 1) (* player.squash.x camera.scale))
   (* player.squash.y camera.scale)
   8
   16))

(local player-mt {:__index player
                  :collide player.collide
                  :update player.update
                  :draw player.draw})

(fn player.new [name colour x y]
  (local image _G.assets.sprites.sprites)
  (local dust _G.assets.sprites.dust)
  (local grass _G.assets.sprites.grass)
  (local quad (if (= colour :white)
                  (love.graphics.newQuad 32 48 16 16 256 256)
                  (love.graphics.newQuad 32 64 16 16 256 256)))
  (local quad-down (if (= colour :white)
                       (love.graphics.newQuad 48 48 16 16 256 256)
                       (love.graphics.newQuad 48 64 16 16 256 256)))
  (local shadow-quad (love.graphics.newQuad 32 80 16 16 256 256))
  (local active false)
  (local type :sheep)
  (local psystem (love.graphics.newParticleSystem dust 300))
  (psystem:setParticleLifetime 2 5)
  (psystem:setEmissionRate 5)
  (psystem:setSizeVariation 1)
  (psystem:setSpeed 0 0 )
  (psystem:setLinearAcceleration -20 -20 20 20)
  (psystem:setRotation -1 1)
  (psystem:setColors 1 1 1 1 1 1 1 0)

  (local eating (love.graphics.newParticleSystem grass 300))
  (eating:setParticleLifetime 2 5)
  (eating:setEmissionRate 0)
  (eating:setSizeVariation 1)
  (eating:setColors 1 1 1 1 1 1 1 0.3)
  (setmetatable {: image : quad : quad-down : shadow-quad : x : y : name : type
                 :rotate 0 :hop 0 :flipped false
                 :squash {:x 1 :y 1}
                 :timer 0
                 : psystem
                 : eating
                 :bpm (/ 1 (/ 160 60 4))
                 :dashcut-down false
                 : active
                 :heavy false
                 :has-axe false
                 :has-feather false
                 :on-finish false
                 :lr 0
                 :ud 0
                 :collide-array (lume.clone default-collide)
                 :speed {:x 0 :y 0 :rate 10 :decay 15
                         :max 2 :max-s2 (/ 2 (math.sqrt 2)) :floor 0.5}}
                player-mt))

player
