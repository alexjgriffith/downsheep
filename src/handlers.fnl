(local state (require :state))

(local gamestate (require :lib.gamestate))

(local col (require :prefab-col))
(local objects (require :objects))
(local quips (require :quips))
(local levels (require :levels))

(fn any-keys-down? []
  (fn check-key-down [keys key]
    (if (-> keys
            (. key)
            (lume.map love.keyboard.isDown)
            (lume.reduce (fn [a b] (or a b))))
        1
        0))

  (fn sign0 [x]
    (if (= x 0) 0
        (> x 0) 1
        (< x 0) -1))

  (local keys state.keys)

  (local keys-down (+ (check-key-down keys :right)
                      (check-key-down keys :left)
                      (check-key-down keys :down)
                      (check-key-down keys :up)
                      (check-key-down keys :dashcut)))
  (~= keys-down 0))


(fn sheep-active []
  (if state.sheep-1.active
      :sheep-1
      :sheep-2))

(fn love.handlers.edit-map [instruction tile tile-size ?previous]
  (local world state.world)
  (when world
    (match instruction
      :add-tile (col.add-tile world state.level.map tile)
      :replace-tile (do
                      (local (items len) (world:getItems))
                      (each [_ item (pairs items)]
                        (when (and (= item.x tile.x) (= item.y tile.y))
                          (world:remove item)))
                      (col.add-tile world state.level.map tile))
      :remove-tile (world:remove tile))))

(fn love.handlers.edit-obj [instruction obj]
  ;; should just call a system! ces.systems.map.edit
  (local world state.world)
  ;; (local level state.entity.level)
  (when (and world state.objects obj)
    (match instruction
      :add-object (do
                    (each [key value (pairs (objects.gen-objects [obj]))]
                      (let [object (. state.objects key)]
                        (when (and object (world:hasItem object)) (world:remove object))
                        (tset state.objects key nil)
                        (tset state.icons key nil))
                      (tset state.objects key value)
                      (world:add value value.x value.y value.w value.h))
                    (local all-icons (objects.gen-icons state.objects))
                    (each [key icon (pairs state.icons)]
                      (tset (. all-icons key) :active icon.active))
                    (tset state :icons all-icons))
      :remove-object (each [_ object (pairs state.objects)]
                     (when (and (= object.x obj.x) (= object.y obj.y))
                       (when (and object (world:hasItem object)) (world:remove object))
                       (tset state.objects object.name nil)
                       (tset state.icons object.name nil))
                     ))))

(fn love.handlers.coin-pickup [coin]
  (local world state.world)
  (when (and world state.objects coin)
    (local object (. state.objects coin.name))
    (object:collide)
    (quips.event :coin-pickup (sheep-active))
    (assets.sounds.cash:play)
    ;;(. levels.list state.current-level)
    (levels.flower-inc state.current-level)
    (when (. state.icons object.name)
      (tset (. state.icons object.name) :active true))
    (when (world:hasItem object)
      (world:remove object))))

(fn love.handlers.pad-up [pad]
  (local world state.world)
  (when (and world state.objects pad)
    (local object (. state.objects pad.name))
    (local (_ len) (world:queryRect object.x object.y object.w object.h
                                    (fn [item] (when (= :sheep item.type) true))))
    (when (= 0 len)
      (each [_ gate (pairs state.objects)]
        (when (and (= :open-gate gate.type) (= gate.colour pad.colour))
          (tset gate :type :gate)
          (tset gate :active true) ;; gate closed
          (gate:collide)))
      (tset object :active false))))

(fn love.handlers.pad-down [pad]
  (local world state.world)
  (when (and world state.objects pad)
    (local object (. state.objects pad.name))
    (when (not pad.active)
      (each [_ gate (pairs state.objects)]
        (when (and (= :gate gate.type) (= gate.colour pad.colour))
          (when (and pad.heavy (not state.sheep-1.heavy))
          (quips.event :not-heavy (sheep-active)))
          (if pad.heavy
              (when state.sheep-1.heavy
                (assets.sounds.bounce:play)
                (quips.event :pad-down (sheep-active))
                (tset gate :type :open-gate)
                (tset gate :active false) ;; gate open
                (gate:collide))
              (do
                (assets.sounds.bounce:play)
                (quips.event :pad-down (sheep-active))
                (tset gate :type :open-gate)
              (tset gate :active false) ;; gate open
              (gate:collide)))))
      (if pad.heavy
          (when state.sheep-1.heavy
            (object:collide))
          (object:collide)))))

(fn love.handlers.step-on-decal [decal]
  (local world state.world)
  (when (and world state.objects)
    (local object (. state.objects decal.name))
    (object:collide)
    (tset state :flash-ui 0.5)
    (when (world:hasItem object)
      (world:remove object))
    (when (. state.icons object.name)
      (tset (. state.icons object.name) :active true))
    (assets.sounds.page:play)
    (match decal.item
      :axe (do (tset state.sheep-1.collide-array :tree "cross")
               (tset state.sheep-2.collide-array :tree "cross")
               (tset state.sheep-1 :has-axe true)
               (tset state.sheep-2 :has-axe true)
               (quips.event :axe (sheep-active)))
      :feather (do (tset state.sheep-1.collide-array :chasm "cross")
                   (tset state.sheep-2.collide-array :chasm "cross")
                   (tset state.sheep-1 :has-feather true)
                   (tset state.sheep-2 :has-feather true)
                   (quips.event :feather (sheep-active)))
      :weight (do (tset state.sheep-1 :heavy true)
                  (tset state.sheep-2 :heavy true)
                  (quips.event :weight (sheep-active))))
    ))

(fn love.handlers.chop-tree [has-axe dash tree]
  (local world state.world)
  (when (and world state.objects)
    (local object (. state.objects tree.name))
    (when (and has-axe dash)
      (assets.sounds.bounce:play)
      (quips.event :chop (sheep-active))
      (tset object :type :cut-tree))))

(fn love.handlers.on-finish [player]
  (local sheep
         (if (= "sheep one" player.name)
             (do (tset state.sheep-1 :on-finish true) state.sheep-1)
             (do (tset state.sheep-2 :on-finish true) state.sheep-2)))
  (when (not sheep.has-eaten-grass)
    (tset sheep :has-eaten-grass true)
    (local name (. {"sheep one" :sheep-1 "sheep two" :sheep-2} sheep.name))
    (quips.event :finish name))
  (when (and state.sheep-1.on-finish state.sheep-2.on-finish (not (any-keys-down?)))
    (quips.event :leave :sheep-1)
    (quips.update state.sheep-1 state.sheep-2 state.camera 0 :overwrite-story)
    (gamestate.switch (require :mode-next-state))))

(fn love.handlers.jump [player _direction]
  (assets.sounds.bounce:play))
