(local coin {})

(fn coin.collide [coin]
  (tset coin :active false)
  (tset coin :index 1)
  (tset coin :time 0)
  coin)

(fn coin.update [coin dt]
  (when (and coin coin.active dt)
    (local max-time (. coin.durrations coin.index))
    (fn inc-counter [value max]
      (local ret (+ value 1))
      (if (> ret max) 1 ret))
    (local time (+ coin.time dt))
    (if (> time max-time)
        (do (tset coin :time (- time  max-time))
            (tset coin :index (inc-counter coin.index (length coin.durrations))))
        (tset coin :time time))))

(fn coin.draw [coin camera]
  (when (and coin coin.active camera)
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    (local quad (. coin :quads coin.index))
    (love.graphics.draw coin.image quad
                        (math.floor (* (+ camera.x coin.x) camera.scale))
                        (math.floor (* (+ camera.y coin.y) camera.scale))
                        0
                        camera.scale
                        camera.scale)))

(local coin-mt {:__index coin
                :update coin.update
                :collide coin.collide
                :draw coin.draw})

;; obj {:type coin :colour :yellow :x 10 :y 10}
(fn coin.new [colour x y]
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos]
    (love.graphics.newQuad pos.x pos.y 8 8 256 256))
  (local quads
         (match colour
     :yellow (lume.map [{:x 40 :y 0} {:x 48 :y 0}  {:x 56 :y 0} {:x 48 :y 0}] newQuad)
     :pink (lume.map [{:x 40 :y 8} {:x 48 :y 8} {:x 56 :y 8} {:x 48 :y 8}] newQuad)
     :orange (lume.map [{:x 40 :y 16} {:x 48 :y 16} {:x 56 :y 16} {:x 48 :y 16}] newQuad)
     :blue (lume.map [{:x 40 :y 24} {:x 48 :y 24} {:x 56 :y 24} {:x 48 :y 24}] newQuad)
     :red (lume.map [{:x 40 :y 32} {:x 48 :y 32} {:x 56 :y 32} {:x 48 :y 32}] newQuad)
     :white (lume.map [{:x 40 :y 40} {:x 48 :y 40} {:x 56 :y 40} {:x 48 :y 40}] newQuad)))
  (setmetatable {: x : y : colour : quads :index (lume.randomchoice [1 2 3 4]) :time (lume.random 0 0.2) : image
                 :w 8 :h 8 :collision-type :cross
                 :type :coin
                 :name (.. :coin " " colour)
                 :durrations [0.40 0.2 0.4 0.2] :active true} coin-mt))

coin
