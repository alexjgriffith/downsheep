(local icon {})

(fn icon.collide [icon])

(fn icon.update [icon dt])

(fn icon.draw [icon]
  (when icon
    ;; (love.graphics.setCanvas)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.draw icon.image (if icon.active icon.quad-down icon.quad-up)
                        icon.x
                        icon.y
                        0
                        2
                        2)))

(local icon-mt {:__index icon
                :update icon.update
                :collide icon.collide
                :draw icon.draw})

;; obj {:type icon :colour :yellow :x 10 :y 10}
(fn icon.new [colour/item x y]
  (local image _G.assets.sprites.sprites)
  (fn newQuad [pos]
    (love.graphics.newQuad pos.x pos.y 16 16 256 256))
  (local quads
         (match colour/item
           :axe     (lume.map [{:x 64 :y 64} {:x 64 :y 48}] newQuad)
           :weight  (lume.map [{:x 80 :y 64} {:x 80 :y 48}] newQuad)
           :feather (lume.map [{:x 96 :y 64} {:x 96 :y 48}] newQuad)
           :yellow  (lume.map [{:x 128 :y 0} {:x 112 :y 96}] newQuad)
           :pink    (lume.map [{:x 128 :y 16} {:x 112 :y 96}] newQuad)
           :orange  (lume.map [{:x 128 :y 32} {:x 112 :y 96}] newQuad)
           :blue    (lume.map [{:x 128 :y 48} {:x 112 :y 96}] newQuad)
           :red     (lume.map [{:x 128 :y 64} {:x 112 :y 96}] newQuad)
           :white   (lume.map [{:x 128 :y 80} {:x 112 :y 96}] newQuad)
           ))
  (local quad-down (. quads 1))
  (local quad-up (. quads 2))
  (setmetatable {: x : y :colour colour/item :item colour/item
                 : quad-down : quad-up : image
                 :w 16 :h 16
                 :type :icon
                 :name (.. :icon " " colour/item)
                 :active false} icon-mt))

icon
