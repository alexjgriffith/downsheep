love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "downsheep", "downsheep"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 320 -- 1280 
   t.window.height = 180 --720
   t.window.vsync = 1
   t.window.centered = true
   t.window.resizable = false
   t.window.borderless= false
   t.window.fullscreentype = "desktop" -- exclusive
   t.window.fullscreen = false
   t.version = "11.3"
   t.gammacorrect = true
end
